-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2020 at 07:09 PM
-- Server version: 5.7.30
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `la_poorna`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8_unicode_ci DEFAULT '1',
  `event_price` varchar(191) COLLATE utf8_unicode_ci DEFAULT '1',
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `event_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `event_id`, `user_id`, `transaction_id`, `amount`, `event_price`, `start_date_time`, `end_date_time`, `status`, `created_at`, `updated_at`, `event_type`) VALUES
(1, 3, 1, '1', '82', '30', '2020-10-03 18:28:00', '2020-10-03 18:28:00', 'sucess', '2020-10-03 07:29:17', '2020-10-03 07:29:17', 'option1');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Fantasy', 'option1', '2020-10-04 22:33:35', '2020-10-04 22:33:35');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(24, 4, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 21),
(25, 4, 'start_date_time', 'timestamp', 'Start Date Time', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 5),
(26, 4, 'end_date_time', 'timestamp', 'End Date Time', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 6),
(27, 4, 'price', 'text', 'Price', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 8),
(28, 4, 'discount', 'text', 'Discount', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 9),
(29, 4, 'image', 'multiple_images', 'Image', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"8\"}}', 17),
(31, 4, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 22),
(32, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 23),
(33, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 24),
(34, 4, 'meeting_id', 'text', 'Meeting Id', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 10),
(35, 4, 'meeting_link', 'text', 'Meeting Link', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 11),
(36, 4, 'meeting_password', 'text', 'Meeting Password', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 12),
(37, 5, 'id', 'text', 'Id', 1, 1, 1, 1, 1, 1, '{}', 1),
(38, 5, 'event_id', 'text', 'Event Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(39, 5, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(40, 5, 'transaction_id', 'hidden', 'Transaction Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(41, 5, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, '{}', 5),
(42, 5, 'event_price', 'text', 'Event Price', 0, 1, 1, 1, 1, 1, '{}', 6),
(43, 5, 'start_date_time', 'timestamp', 'Start Date Time', 0, 1, 1, 1, 1, 1, '{}', 7),
(44, 5, 'end_date_time', 'timestamp', 'End Date Time', 0, 0, 1, 1, 1, 1, '{}', 8),
(45, 5, 'status', 'hidden', 'Status', 0, 1, 1, 1, 1, 1, '{}', 9),
(46, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 10),
(47, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(48, 5, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 12),
(50, 5, 'event_booking_belongsto_event_relationship', 'relationship', 'events', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Event\",\"table\":\"events\",\"type\":\"belongsTo\",\"column\":\"event_id\",\"key\":\"id\",\"label\":\"event_name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(51, 4, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 2),
(52, 4, 'max_seats', 'text', 'Max Seats', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 14),
(53, 4, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Event Status\",\"options\":{\"option1\":\"Active\",\"option2\":\"Inactive\"},\"display\":{\"width\":\"6\"}}', 15),
(54, 5, 'event_type', 'select_dropdown', 'Event Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Event Type\",\"options\":{\"option1\":\"default\",\"option2\":\"utsava\"}}', 13),
(55, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(56, 6, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 2),
(57, 6, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 9),
(58, 6, 'start_date_time', 'timestamp', 'Start Date Time', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 4),
(59, 6, 'end_date_time', 'timestamp', 'End Date Time', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 5),
(60, 6, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Event Status\",\"options\":{\"option1\":\"Active\",\"option2\":\"Inactive\"},\"display\":{\"width\":\"6\"}}', 10),
(61, 6, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 6),
(62, 6, 'discount', 'text', 'Discount', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 7),
(63, 6, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 13),
(64, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(65, 6, 'image', 'multiple_images', 'Image', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"8\"}}', 11),
(66, 6, 'video', 'file', 'Video', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 12),
(67, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(68, 7, 'utsava_id', 'select_dropdown', 'Utsava Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(69, 7, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{}', 6),
(70, 7, 'start_time', 'timestamp', 'Start Time', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 4),
(71, 7, 'end_time', 'timestamp', 'End Time', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 5),
(72, 7, 'image', 'multiple_images', 'Image', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 7),
(73, 7, 'video', 'file', 'Video', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 8),
(74, 7, 'felicitator', 'text', 'Felicitator', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 12),
(75, 7, 'meeiting_id', 'text', 'Meeiting Id', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 9),
(76, 7, 'meeting_password', 'text', 'Meeting Password', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 10),
(77, 7, 'meeting_link', 'text', 'Meeting Link', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"4\"}}', 11),
(78, 7, 'max_seats', 'text', 'Max Seats', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 13),
(79, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 14),
(80, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(82, 7, 'utsava_session_belongsto_utsava_relationship', 'relationship', 'utsavas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Utsava\",\"table\":\"utsavas\",\"type\":\"belongsTo\",\"column\":\"utsava_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"cart\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(83, 6, 'permalink', 'text', 'Permalink', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"},\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(84, 4, 'permalink', 'text', 'Permalink', 0, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"display\":{\"width\":\"6\"}}', 4),
(85, 4, 'venue', 'select_dropdown', 'Venue', 0, 0, 1, 1, 1, 1, '{\"default\":\"Select Venue\",\"options\":{\"option1\":\"fb live\",\"option2\":\"insta live\",\"option3\":\"Online  workshop\",\"option4\":\"Offline workshop\",\"option5\":\"Online series\",\"option6\":\"LMS\",\"option7\":\"Whatsapp series\"},\"display\":{\"width\":\"6\"}}', 13),
(86, 6, 'venue', 'text', 'Venue', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 8),
(87, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(88, 8, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(89, 8, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select categorie status\",\"options\":{\"option1\":\"Active\",\"option2\":\"Inactive\"}}', 3),
(90, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(91, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(92, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(93, 10, 'event_id', 'text', 'Event Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(94, 10, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(95, 10, 'transaction_id', 'text', 'Transaction Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(96, 10, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, '{}', 5),
(97, 10, 'event_price', 'text', 'Event Price', 0, 1, 1, 1, 1, 1, '{}', 7),
(98, 10, 'start_date_time', 'timestamp', 'Start Date Time', 0, 1, 1, 1, 1, 1, '{}', 8),
(99, 10, 'end_date_time', 'timestamp', 'End Date Time', 0, 1, 1, 1, 1, 1, '{}', 9),
(100, 10, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 10),
(101, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 11),
(102, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(104, 10, 'event_type', 'select_dropdown', 'Event Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Event Type\",\"options\":{\"option1\":\"Nomal\",\"option2\":\"Utsava\"}}', 6),
(105, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(106, 11, 'category_id', 'text', 'Category Name', 0, 0, 0, 0, 0, 0, '{}', 3),
(107, 11, 'code', 'text', 'Code', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 6),
(108, 11, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 4),
(109, 11, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 5),
(110, 11, 'front_image', 'image', 'Front Image', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 7),
(111, 11, 'other_image', 'multiple_images', 'Other Image', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 8),
(112, 11, 'author', 'text', 'Author', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 9),
(113, 11, 'publication', 'text', 'Publication', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 10),
(114, 11, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{}', 13),
(115, 11, 'discount', 'text', 'Discount', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 11),
(116, 11, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Event Status\",\"options\":{\"option1\":\"Pending\",\"option2\":\"success\",\"option3\":\"cancelled\"},\"display\":{\"width\":\"6\"}}', 12),
(117, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 14),
(118, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(119, 11, 'product_belongsto_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"},\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bookings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(120, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(121, 12, 'cart_id', 'text', 'Cart Id', 0, 0, 1, 1, 1, 1, '{}', 2),
(122, 12, 'user_id', 'text', 'User Id', 0, 0, 1, 1, 1, 1, '{}', 3),
(123, 12, 'purchase_id', 'text', 'Purchase Id', 0, 0, 1, 1, 1, 1, '{}', 4),
(124, 12, 'transaction_id', 'text', 'Transaction Id', 0, 0, 1, 1, 1, 1, '{}', 5),
(125, 12, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, '{}', 6),
(126, 12, 'product_price', 'text', 'Product Price', 0, 1, 1, 1, 1, 1, '{}', 7),
(127, 12, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Event Status\",\"options\":{\"option1\":\"Pending\",\"option2\":\"success\",\"option3\":\"cancelled\"},\"display\":{\"width\":\"6\"}}', 8),
(128, 12, 'coupon_applied', 'text', 'Coupon Applied', 0, 1, 1, 1, 1, 1, '{}', 9),
(129, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 10),
(130, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(131, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(132, 13, 'product_id', 'text', 'Product Name', 0, 0, 0, 0, 0, 0, '{}', 2),
(133, 13, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 3),
(134, 13, 'discount', 'text', 'Discount', 0, 1, 1, 1, 1, 1, '{}', 4),
(135, 13, 'from_datetime', 'timestamp', 'From Datetime', 0, 1, 1, 1, 1, 1, '{}', 5),
(136, 13, 'to_datetime', 'timestamp', 'To Datetime', 0, 1, 1, 1, 1, 1, '{}', 6),
(137, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 8),
(138, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(139, 13, 'discount_belongsto_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bookings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(140, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(141, 14, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 2),
(142, 14, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 4),
(143, 14, 'code', 'text', 'Code', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 5),
(144, 14, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 6),
(145, 14, 'valid_from', 'timestamp', 'Valid From', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 7),
(146, 14, 'valid_to', 'timestamp', 'Valid To', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 8),
(147, 14, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Coupon Status\",\"options\":{\"option1\":\"Active\",\"option2\":\"Inactive\"},\"display\":{\"width\":\"6\"}}', 9),
(148, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 10),
(149, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(150, 14, 'coupon_hasone_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"},\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"bookings\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(151, 15, 'id', 'hidden', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(152, 15, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(153, 15, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(154, 15, 'designation', 'text', 'Designation', 0, 1, 1, 1, 1, 1, '{}', 4),
(155, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 5),
(156, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(157, 4, 'cover_image', 'image', 'Cover Image', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"4\"}}', 16),
(158, 4, 'location', 'text', 'Location', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 18),
(159, 4, 'facilitator_id', 'text', 'Facilitator Id', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"},\"display\":{\"width\":\"6\"}}', 3),
(160, 4, 'type_price', 'select_dropdown', 'Type Price', 0, 1, 1, 1, 1, 1, '{\"default\":\"Select Price Type\",\"options\":{\"option1\":\"complimentary\",\"option2\":\"paid\"},\"display\":{\"width\":\"6\"}}', 7),
(161, 4, 'video_one', 'text', 'Video One', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 19),
(162, 4, 'video_two', 'text', 'Video Two', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":\"6\"}}', 20);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-08-16 11:16:50', '2020-08-16 11:16:50'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-08-16 11:16:50', '2020-08-16 11:16:50'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-08-16 11:16:50', '2020-08-16 11:16:50'),
(4, 'events', 'events', 'Event', 'Events', 'voyager-news', 'App\\Event', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-08-18 22:01:11', '2020-12-11 12:30:21'),
(5, 'event_bookings', 'event-bookings', 'Event Booking', 'Event Bookings', NULL, 'App\\EventBooking', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":\"event_id\",\"scope\":null}', '2020-08-23 04:26:27', '2020-09-10 21:43:54'),
(6, 'utsavas', 'utsavas', 'Utsava', 'Utsavas', NULL, 'App\\Utsava', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-11 10:48:22', '2020-09-22 12:06:46'),
(7, 'utsava_sessions', 'utsava-sessions', 'Utsava Session', 'Utsava Sessions', NULL, 'App\\UtsavaSession', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-11 10:54:26', '2020-09-21 22:35:59'),
(8, 'categories', 'categories', 'Category', 'Categories', NULL, 'App\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-22 21:40:18', '2020-10-04 22:32:47'),
(10, 'bookings', 'bookings', 'Booking', 'Bookings', NULL, 'App\\Booking', NULL, '\\App\\Http\\Controllers\\VoyagerBookingController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 21:29:46', '2020-10-03 07:28:21'),
(11, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-04 22:22:58', '2020-10-05 21:54:44'),
(12, 'orders', 'orders', 'Order', 'Orders', NULL, 'App\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-05 22:03:23', '2020-10-05 22:05:46'),
(13, 'discounts', 'discounts', 'Discount', 'Discounts', NULL, 'App\\Discount', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-05 22:09:08', '2020-10-05 22:10:54'),
(14, 'coupons', 'coupons', 'Coupon', 'Coupons', NULL, 'App\\Coupon', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-05 22:13:47', '2020-10-05 22:42:03'),
(15, 'facilitators', 'facilitators', 'Facilitator', 'Facilitators', NULL, 'App\\Facilitator', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-07 12:59:36', '2020-12-11 12:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `from_datetime` datetime DEFAULT NULL,
  `to_datetime` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `cover_image` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meeting_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeting_link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeting_password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_seats` int(11) DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permalink` text COLLATE utf8_unicode_ci,
  `venue` text COLLATE utf8_unicode_ci,
  `location` varchar(187) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facilitator_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_price` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_one` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_two` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facilitators`
--

CREATE TABLE `facilitators` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `designation` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `facilitators`
--

INSERT INTO `facilitators` (`id`, `name`, `image`, `designation`, `created_at`, `updated_at`) VALUES
(1, 'shiv', 'facilitators\\December2020\\pcBFZUMfJhKCyNWTnQpZ.png', 'test', '2020-12-08 17:43:00', '2020-12-08 12:13:52'),
(2, 'Dry', 'facilitators\\December2020\\ZGVhgHIwWBVGSuMA2vs0.png', 'test', '2020-12-08 12:18:47', '2020-12-08 12:18:47');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-08-16 11:16:51', '2020-08-16 11:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-08-16 11:16:51', '2020-08-16 11:16:51', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 7, '2020-08-16 11:16:51', '2020-12-07 13:01:01', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-08-16 11:16:51', '2020-08-16 11:16:51', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-08-16 11:16:51', '2020-08-16 11:16:51', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2020-08-16 11:16:51', '2020-12-07 13:01:01', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-08-16 11:16:51', '2020-08-18 22:14:05', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-08-16 11:16:51', '2020-08-18 22:14:05', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-08-16 11:16:51', '2020-08-18 22:14:05', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-08-16 11:16:51', '2020-08-18 22:14:05', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2020-08-16 11:16:51', '2020-12-07 13:01:01', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-08-16 11:16:53', '2020-08-18 22:14:05', 'voyager.hooks', NULL),
(12, 1, 'Events', '', '_self', 'voyager-news', NULL, 14, 1, '2020-08-18 22:01:11', '2020-08-23 04:28:22', 'voyager.events.index', NULL),
(13, 1, 'Bookings', '', '_self', 'voyager-window-list', '#000000', 14, 4, '2020-08-23 04:26:27', '2020-09-28 21:30:56', 'voyager.bookings.index', 'null'),
(14, 1, 'Events', '', '_self', 'voyager-list', '#000000', NULL, 5, '2020-08-23 04:28:10', '2020-12-07 13:01:01', NULL, ''),
(15, 1, 'Utsavas', '', '_self', 'voyager-news', '#000000', 14, 2, '2020-09-11 10:48:23', '2020-09-11 10:57:25', 'voyager.utsavas.index', 'null'),
(16, 1, 'Utsava Session', '', '_self', 'voyager-news', '#000000', 14, 3, '2020-09-11 10:54:27', '2020-09-11 10:56:38', 'voyager.utsava-sessions.index', 'null'),
(18, 1, 'Book Shop', '#', '_self', 'voyager-logbook', '#000000', NULL, 6, '2020-09-22 21:43:23', '2020-12-07 13:01:01', NULL, ''),
(19, 1, 'Category', '/admin/categories', '_self', 'voyager-documentation', '#000000', 18, 1, '2020-09-22 21:45:31', '2020-09-22 21:45:48', NULL, ''),
(21, 1, 'Products', '', '_self', 'voyager-trees', '#000000', 18, 2, '2020-10-04 22:22:58', '2020-10-05 22:45:12', 'voyager.products.index', 'null'),
(22, 1, 'Orders', '', '_self', 'voyager-new', '#000000', 18, 3, '2020-10-05 22:03:24', '2020-10-05 22:45:37', 'voyager.orders.index', 'null'),
(23, 1, 'Discounts', '', '_self', 'voyager-pen', '#000000', 18, 4, '2020-10-05 22:09:09', '2020-10-05 22:46:05', 'voyager.discounts.index', 'null'),
(24, 1, 'Coupons', '', '_self', 'voyager-pen', '#000000', 18, 5, '2020-10-05 22:13:47', '2020-10-05 22:46:14', 'voyager.coupons.index', 'null'),
(25, 1, 'Facilitators', '', '_self', 'voyager-people', '#000000', NULL, 4, '2020-12-07 12:59:36', '2020-12-11 12:27:09', 'voyager.facilitators.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `status` varchar(193) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coupon_applied` varchar(193) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(2, 'browse_bread', NULL, '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(3, 'browse_database', NULL, '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(4, 'browse_media', NULL, '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(5, 'browse_compass', NULL, '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(6, 'browse_menus', 'menus', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(7, 'read_menus', 'menus', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(8, 'edit_menus', 'menus', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(9, 'add_menus', 'menus', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(10, 'delete_menus', 'menus', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(11, 'browse_roles', 'roles', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(12, 'read_roles', 'roles', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(13, 'edit_roles', 'roles', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(14, 'add_roles', 'roles', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(15, 'delete_roles', 'roles', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(16, 'browse_users', 'users', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(17, 'read_users', 'users', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(18, 'edit_users', 'users', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(19, 'add_users', 'users', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(20, 'delete_users', 'users', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(21, 'browse_settings', 'settings', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(22, 'read_settings', 'settings', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(23, 'edit_settings', 'settings', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(24, 'add_settings', 'settings', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(25, 'delete_settings', 'settings', '2020-08-16 11:16:52', '2020-08-16 11:16:52'),
(26, 'browse_hooks', NULL, '2020-08-16 11:16:53', '2020-08-16 11:16:53'),
(27, 'browse_events', 'events', '2020-08-18 22:01:11', '2020-08-18 22:01:11'),
(28, 'read_events', 'events', '2020-08-18 22:01:11', '2020-08-18 22:01:11'),
(29, 'edit_events', 'events', '2020-08-18 22:01:11', '2020-08-18 22:01:11'),
(30, 'add_events', 'events', '2020-08-18 22:01:11', '2020-08-18 22:01:11'),
(31, 'delete_events', 'events', '2020-08-18 22:01:11', '2020-08-18 22:01:11'),
(32, 'browse_event_bookings', 'event_bookings', '2020-08-23 04:26:27', '2020-08-23 04:26:27'),
(33, 'read_event_bookings', 'event_bookings', '2020-08-23 04:26:27', '2020-08-23 04:26:27'),
(34, 'edit_event_bookings', 'event_bookings', '2020-08-23 04:26:27', '2020-08-23 04:26:27'),
(35, 'add_event_bookings', 'event_bookings', '2020-08-23 04:26:27', '2020-08-23 04:26:27'),
(36, 'delete_event_bookings', 'event_bookings', '2020-08-23 04:26:27', '2020-08-23 04:26:27'),
(37, 'browse_utsavas', 'utsavas', '2020-09-11 10:48:23', '2020-09-11 10:48:23'),
(38, 'read_utsavas', 'utsavas', '2020-09-11 10:48:23', '2020-09-11 10:48:23'),
(39, 'edit_utsavas', 'utsavas', '2020-09-11 10:48:23', '2020-09-11 10:48:23'),
(40, 'add_utsavas', 'utsavas', '2020-09-11 10:48:23', '2020-09-11 10:48:23'),
(41, 'delete_utsavas', 'utsavas', '2020-09-11 10:48:23', '2020-09-11 10:48:23'),
(42, 'browse_utsava_sessions', 'utsava_sessions', '2020-09-11 10:54:27', '2020-09-11 10:54:27'),
(43, 'read_utsava_sessions', 'utsava_sessions', '2020-09-11 10:54:27', '2020-09-11 10:54:27'),
(44, 'edit_utsava_sessions', 'utsava_sessions', '2020-09-11 10:54:27', '2020-09-11 10:54:27'),
(45, 'add_utsava_sessions', 'utsava_sessions', '2020-09-11 10:54:27', '2020-09-11 10:54:27'),
(46, 'delete_utsava_sessions', 'utsava_sessions', '2020-09-11 10:54:27', '2020-09-11 10:54:27'),
(47, 'browse_categories', 'categories', '2020-09-22 21:40:18', '2020-09-22 21:40:18'),
(48, 'read_categories', 'categories', '2020-09-22 21:40:18', '2020-09-22 21:40:18'),
(49, 'edit_categories', 'categories', '2020-09-22 21:40:18', '2020-09-22 21:40:18'),
(50, 'add_categories', 'categories', '2020-09-22 21:40:18', '2020-09-22 21:40:18'),
(51, 'delete_categories', 'categories', '2020-09-22 21:40:18', '2020-09-22 21:40:18'),
(52, 'browse_bookings', 'bookings', '2020-09-28 21:29:46', '2020-09-28 21:29:46'),
(53, 'read_bookings', 'bookings', '2020-09-28 21:29:46', '2020-09-28 21:29:46'),
(54, 'edit_bookings', 'bookings', '2020-09-28 21:29:46', '2020-09-28 21:29:46'),
(55, 'add_bookings', 'bookings', '2020-09-28 21:29:46', '2020-09-28 21:29:46'),
(56, 'delete_bookings', 'bookings', '2020-09-28 21:29:46', '2020-09-28 21:29:46'),
(57, 'browse_products', 'products', '2020-10-04 22:22:58', '2020-10-04 22:22:58'),
(58, 'read_products', 'products', '2020-10-04 22:22:58', '2020-10-04 22:22:58'),
(59, 'edit_products', 'products', '2020-10-04 22:22:58', '2020-10-04 22:22:58'),
(60, 'add_products', 'products', '2020-10-04 22:22:58', '2020-10-04 22:22:58'),
(61, 'delete_products', 'products', '2020-10-04 22:22:58', '2020-10-04 22:22:58'),
(62, 'browse_orders', 'orders', '2020-10-05 22:03:23', '2020-10-05 22:03:23'),
(63, 'read_orders', 'orders', '2020-10-05 22:03:23', '2020-10-05 22:03:23'),
(64, 'edit_orders', 'orders', '2020-10-05 22:03:23', '2020-10-05 22:03:23'),
(65, 'add_orders', 'orders', '2020-10-05 22:03:23', '2020-10-05 22:03:23'),
(66, 'delete_orders', 'orders', '2020-10-05 22:03:23', '2020-10-05 22:03:23'),
(67, 'browse_discounts', 'discounts', '2020-10-05 22:09:09', '2020-10-05 22:09:09'),
(68, 'read_discounts', 'discounts', '2020-10-05 22:09:09', '2020-10-05 22:09:09'),
(69, 'edit_discounts', 'discounts', '2020-10-05 22:09:09', '2020-10-05 22:09:09'),
(70, 'add_discounts', 'discounts', '2020-10-05 22:09:09', '2020-10-05 22:09:09'),
(71, 'delete_discounts', 'discounts', '2020-10-05 22:09:09', '2020-10-05 22:09:09'),
(72, 'browse_coupons', 'coupons', '2020-10-05 22:13:47', '2020-10-05 22:13:47'),
(73, 'read_coupons', 'coupons', '2020-10-05 22:13:47', '2020-10-05 22:13:47'),
(74, 'edit_coupons', 'coupons', '2020-10-05 22:13:47', '2020-10-05 22:13:47'),
(75, 'add_coupons', 'coupons', '2020-10-05 22:13:47', '2020-10-05 22:13:47'),
(76, 'delete_coupons', 'coupons', '2020-10-05 22:13:47', '2020-10-05 22:13:47'),
(77, 'browse_facilitators', 'facilitators', '2020-12-07 12:59:36', '2020-12-07 12:59:36'),
(78, 'read_facilitators', 'facilitators', '2020-12-07 12:59:36', '2020-12-07 12:59:36'),
(79, 'edit_facilitators', 'facilitators', '2020-12-07 12:59:36', '2020-12-07 12:59:36'),
(80, 'add_facilitators', 'facilitators', '2020-12-07 12:59:36', '2020-12-07 12:59:36'),
(81, 'delete_facilitators', 'facilitators', '2020-12-07 12:59:36', '2020-12-07 12:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(2, 3),
(3, 1),
(3, 3),
(4, 1),
(4, 3),
(5, 1),
(5, 3),
(6, 1),
(6, 3),
(7, 1),
(7, 3),
(8, 1),
(8, 3),
(9, 1),
(9, 3),
(10, 1),
(10, 3),
(11, 1),
(11, 3),
(12, 1),
(12, 3),
(13, 1),
(13, 3),
(14, 1),
(14, 3),
(15, 1),
(15, 3),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 1),
(20, 3),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(23, 3),
(24, 1),
(24, 3),
(25, 1),
(25, 3),
(26, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(39, 3),
(40, 1),
(40, 3),
(41, 1),
(41, 3),
(42, 1),
(42, 3),
(43, 1),
(43, 3),
(44, 1),
(44, 3),
(45, 1),
(45, 3),
(46, 1),
(46, 3),
(47, 1),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(49, 3),
(50, 1),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(56, 3),
(57, 1),
(57, 3),
(58, 1),
(58, 3),
(59, 1),
(59, 3),
(60, 1),
(60, 3),
(61, 1),
(61, 3),
(62, 1),
(62, 3),
(63, 1),
(63, 3),
(64, 1),
(64, 3),
(65, 1),
(65, 3),
(66, 1),
(66, 3),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 1),
(70, 3),
(71, 1),
(71, 3),
(72, 1),
(72, 3),
(73, 1),
(73, 3),
(74, 1),
(74, 3),
(75, 1),
(75, 3),
(76, 1),
(76, 3),
(77, 1),
(77, 3),
(78, 1),
(78, 3),
(79, 1),
(79, 3),
(80, 1),
(80, 3),
(81, 1),
(81, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `front_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_image` mediumtext COLLATE utf8_unicode_ci,
  `author` int(11) DEFAULT NULL,
  `publication` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `discount` float DEFAULT NULL,
  `status` varchar(194) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(2, 'user', 'Normal User', '2020-08-16 11:16:51', '2020-08-16 11:16:51'),
(3, 'Developer', 'developer', '2020-10-05 22:48:12', '2020-10-05 22:48:12');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `details` text COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Poorna', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `synced` int(11) DEFAULT NULL,
  `number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `lat` double DEFAULT NULL,
  `city` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `synced`, `number`, `address`, `lat`, `city`, `status`) VALUES
(1, 3, 'Admin', 'admin_poorna@gmail.com', 'users\\August2020\\r0N5cqcO7OkHVhUUtotj.png', '2020-10-20 16:16:20', '$2y$10$sP.B6pfAfLdNrG6BQDfCJOzGJkancPzid6kj3TV3P.MTU729BeTY6', '1JqZN8OrUWUFLivdlvTNRrM8gcUASZG5nlMfr9Xwfwgq33Bf4M3LKSoHhMKT', '{\"locale\":\"en\"}', '2020-08-18 21:09:30', '2020-08-18 21:14:10', NULL, NULL, NULL, NULL, NULL, 1),
(2, 2, 'shiv', 'shivendra.shukla21@gmail.com', 'users/default.png', '2020-10-19 16:16:15', '$2y$10$To4e5EhR8l.aBwtb17p5neb7UQluOUEYmP6GlzTxKz.UaMOpxOq3y', NULL, NULL, '2020-10-09 11:15:55', '2020-10-09 11:15:56', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utsavas`
--

CREATE TABLE `utsavas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  `video` longtext COLLATE utf8_unicode_ci,
  `permalink` text COLLATE utf8_unicode_ci,
  `venue` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utsava_sessions`
--

CREATE TABLE `utsava_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `utsava_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  `video` longtext COLLATE utf8_unicode_ci,
  `felicitator` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeiting_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeting_password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meeting_link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_seats` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilitators`
--
ALTER TABLE `facilitators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `utsavas`
--
ALTER TABLE `utsavas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utsava_sessions`
--
ALTER TABLE `utsava_sessions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `facilitators`
--
ALTER TABLE `facilitators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `utsavas`
--
ALTER TABLE `utsavas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utsava_sessions`
--
ALTER TABLE `utsava_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
