<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

	protected $table = "events";
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'start_date_time', 'end_date_time', 'price', 'discount', 'image', 'video', 'max_seats', 'meeting_id', 'meeting_password', 'meeting_link'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date_time' => 'datetime',
        'end_date_time' => 'datetime',
        'updated_at' => 'datetime',
    ];
    
}
