<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Storage;
use App\MyCart;
use Session;

class ProductsController extends Controller {

  public function single(Product $product){
    return view('products.product-single', compact('product'));
  }

  public function addToCart(Product $product, Request $request){
    $oldCart = Session::has('cart') ? Session::get('cart') : null;
    $qty = $request->qty ? $request->qty : 1;
    $cart = new MyCart($oldCart);
    $cart->addProduct($product, $qty);
    Session::put('cart', $cart);
    //return view('products.checkout');//back()->with('message', "Product $product->title has been successfully added to Cart");
    return redirect()->route('orders');
  }


}
