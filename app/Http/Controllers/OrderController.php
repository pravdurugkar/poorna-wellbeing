<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\storeShipping;
use Session;

class OrderController extends Controller {
  public function index() {
		if (!Session::has('cart') || empty(Session::get('cart')->getContents())) {
			return redirect('products/1')->with('message', 'No Products in the Cart');
		}
		$cart = Session::get('cart');
		//dd($cart);
		return view('products.checkout', compact('cart'));
	}

	public function store (storeShipping $request) {
		echo "<b>Success</b>"; die;
	}  
}
