<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/*use Illuminate\Support\Facades\DB;*/
use App\Product;

class Products extends Controller {
  public function index (Request $request,$category_id = null) {
  	if ($category_id) :
  		$products = Product::where('category_id',$category_id)->get();
  	else :
    	$products = Product::all();
    endif;
  	return view('ecommerce.products')->with('products',$products);
  }
}
