<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Booking;

class EventsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.events');
    }

    public function page(){
        $events = Event::all();
    	return view('pages.events', ["events" => $events]);
    }

    public function singleEvent($permalink){
        if ($permalink) {
            $event = Event::where('permalink', $permalink)->first();
            
            if( !empty($event)) {
                return view('pages.event', ["event" => $event]);
            }
        }
        
        return redirect('events');
    }

    public function bookingForm($permalink) {
        if ($permalink) {
            $event = Event::where('permalink', $permalink)->first();
            
            if( !empty($event)) {
                return view('pages.eventBooking', ["event" => $event]);
            }
        }
        
        return redirect('events');
    }

    public function bookEvent(Request $request) {
        //if ($permalink) {
            //$event = Event::where('permalink', $permalink)->first();
            
            //if( !empty($event)) {
                //return view('pages.eventBooking', ["event" => $event]);

            //}
        //}


        $booking = new Booking();

        // $booking->
        
        echo 'Testing event booking';
        //return redirect('events');
    }
}