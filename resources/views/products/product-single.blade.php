<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <!-- Css added for input number -->
  <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
  @include('nav.menu')
</div>
<section class="section-event_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="event-banner_head">
          <p>Poorna Wellbeing Event Management</p>
          <h1>Product Single page</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-event_listing">
  <div class="container">
    <div class="product-post event-post pb-5">
      <div class="row">
        <div class="col-md-5">
           <div class="image-viewer">
            <div class="main-image">
              <img src="{{ asset('images/event/event-img-06.jpg') }}"/>
               <!-- <img src="{{ asset('images/event/'.$product->front_image) }}"/> -->
            </div>
            <div class="secondary-images">
              <div class="secondary-image">
                <img src="{{ asset('images/event/event-img-06.jpg') }}"/>
                <!-- <img src="{{ asset('images/event/'.$product->front_image) }}"/> -->
              </div>
               <div class="secondary-image">
                <img src="{{ asset('images/event/event-img-06.jpg') }}"/>
                <!-- <img src="{{ asset('images/event/'.$product->front_image) }}"/> -->
              </div>
              <div class="secondary-image">
                <img src="{{ asset('images/event/event-img-05.jpg') }}"/>
                <!-- <img src="{{ asset('images/event/'.$product->other_image) }}"/> -->
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <h1 class="product-title"><a href="#">{{$product->name}}</a></h1>
          <div class="rating1">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <span class="pl-20">(1 customer review)</span>
          </div>
          <p class="product-price">Rs. {{$product->price}}</p>
          <div class="">
            <p>{{$product->author}}</p>
            <p>{{$product->publication}}</p>
          </div>
          <div class="description">
            <p>{{$product->description}}</p>
          </div>
          <div class="row">
            <form method="post" action="{{route('products.addToCart', $product)}}">
              <div class="col-md-3">
                <p class="m-3 custom-text-number">
                  @csrf
                  <input type="number" pattern="[0-9]*" id="spinner" name="qty" value="1" min="1" max="200" step="1" oninput="maxLengthCheck(this)" maxlength="3">
                </p>
              </div>
              <div class="col-md-9">
                <button type="submit" class="btn btn-primary add-to-cart-btn">Buy Now</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
</section>

<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- Js added for input number -->
<script type="text/javascript">
  function spinner() {
  //  SPINNER
  $("#spinner").spinner();
  
  //  INPUT ONLY NUMBERS
  $('#spinner').keyup(function () { 
     this.value = this.value.replace(/[^0-9]/g,'');
  });
}
// INPUT NUMBER MAX LENGHT
function maxLengthCheck(object) {
  if (object.value.length > object.maxLength)
    object.value = object.value.slice(0, object.maxLength)
}
window.onload = spinner;
</script>
<!-- Js added for input number -->
<script type="text/javascript">
  class ImageViewer {
  constructor(selector) {
    this.selector = selector;
    $(this.secondaryImages).click(() => this.setMainImage(event));
    $(this.mainImage).click(() => this.showLightbox(event));
    $(this.lightboxClose).click(() => this.hideLightbox(event));
  }
  
  get secondaryImageSelector() {
    return '.secondary-image';
  }
  
  get mainImageSelector() {
    return '.main-image';
  }
  
  get lightboxImageSelector() {
    return '.lightbox';
  }
  
  get lightboxClose() {
    return '.lightbox-controls-close';
  }
  
  get secondaryImages() {
    var secondaryImages = $(this.selector).find(this.secondaryImageSelector).find('img')
    return secondaryImages;
  }
  
  get mainImage() {
    var mainImage = $(this.selector).find(this.mainImageSelector);
    return mainImage;
  }
  
  get lightboxImage() {
    var lightboxImage = $(this.lightboxImageSelector);
    return lightboxImage;
  }
  
  setLightboxImage(event){
    var src = this.getEventSrc(event);
    this.setSrc(this.lightboxImage, src);
  }
  
  setMainImage(event){
    var src = this.getEventSrc(event);
    this.setSrc(this.mainImage, src);
  }
  
  getSrc(node){
    var image = $(node).find('img');
  }
  
  setSrc(node, src){
    var image = $(node).find('img')[0];
    image.src = src;
  }
  
  getEventSrc(event){
    return event.target.src;
  }
  
  showLightbox(event){
    this.setLightboxImage(event);
    $(this.lightboxImageSelector).addClass('show');
  }
  
  hideLightbox(){
    $(this.lightboxImageSelector).removeClass('show');
  }
}

new ImageViewer('.image-viewer');
</script>
<style type="text/css">
  .pb-header-main .navbar-nav>li>a{color: #fff;}
  /*canvas#bg {
      background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
  }*/
</style>

</body>
</html>
