<section class="section section-testimonials testimonials-bg" id="section-testimonials">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 wow slideInDown" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="heading-box text-center">
                    <h2 class="mb-25 text-green">TESTIMONIALS</h2>
                </div>
            </div>
        </div>
        <div class="row testimonials-row ">
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Kavita</h3>
                    <div class="testi-text">
                        <iframe width="100%" height="180" src="https://www.youtube.com/embed/TfJ0517ZChY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Kamala</h3>
                    <div class="testi-text">
                        <iframe width="100%" height="180" src="https://www.youtube.com/embed/-YALmLTSnHc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Avantika</h3>
                    <div class="testi-text testi-video">
                        <iframe width="100%" height="180" src="https://www.youtube.com/embed/uhciMcb4N04" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <!-- <div class="col-xs-12 col-sm-6 col-md-6 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Other flavours </h3>
                    <div class="testi-text testi-video">
                        <iframe width="100%" height="180" src="https://www.youtube.com/embed/Y7fvRMsrTVA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="row">
            <div class="scolldown pw-purplecroll pw-scrollDown scolldown1">
                <figure><button type="button" scroll-data-id="#section-contact-hm"><img data-src="images/arrow-2.png" alt="" class="lazy-loaded" data-lazy-type="image" src="images/arrow-2.png"></button></figure>
            </div>
        </div>
        <div class="row testimonials-row ">
            <!-- <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Kavita</h3>
                    <div class="testi-text">
                        <iframe width="100%" height="180" src="https://www.youtube.com/embed/TfJ0517ZChY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div> -->
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Kamala</h3>
                    <div class="testi-text">
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                         <a href="#." class="btn btn-testimonials">Read More</a>
                    </div>
                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Utsava participants </h3>
                    <div class="testi-text testi-video">
                       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                       <a href="#." class="btn btn-testimonials">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="testimonials-box">
                    <div class="testi-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    <h3 class="testi-name">Utsava participants </h3>
                    <div class="testi-text testi-video">
                       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                       <a href="#." class="btn btn-testimonials">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>