<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <!-- Css added for input number -->
  <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  <style type="text/css">
    .form-control{
      margin-bottom: 20px;
    }
  </style>
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
  @include('nav.menu')
</div>
<section class="section-event_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="event-banner_head">
          <p>Poorna Wellbeing Event Management</p>
          <h1>Product Single page</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-event_listing">
  <div class="container">
    <div class="row">
      <div class="col-md-4 order-md-2 mb-4">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-muted">Your cart</span>
          <span class="badge badge-secondary badge-pill">3</span>
        </h4>
        <ul class="list-group mb-3">
          <li class="list-group-item d-flex justify-content-between lh-condensed">
            <div>
              <h6 class="my-0">Product name</h6>
              <small class="text-muted">Brief description</small>
            </div>
            <span class="text-muted">$12</span>
          </li>
          <li class="list-group-item d-flex justify-content-between lh-condensed">
            <div>
              <h6 class="my-0">Second product</h6>
              <small class="text-muted">Brief description</small>
            </div>
            <span class="text-muted">$8</span>
          </li>
          <li class="list-group-item d-flex justify-content-between lh-condensed">
            <div>
              <h6 class="my-0">Third item</h6>
              <small class="text-muted">Brief description</small>
            </div>
            <span class="text-muted">$5</span>
          </li>
          <li class="list-group-item d-flex justify-content-between bg-light">
            <div class="text-success">
              <h6 class="my-0">Promo code</h6>
              <small>EXAMPLECODE</small>
            </div>
            <span class="text-success">-$5</span>
          </li>
          <li class="list-group-item d-flex justify-content-between">
            <span>Total (USD)</span>
            <strong>$20</strong>
          </li>
        </ul>

        <form class="card p-2">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Promo code">
            <div class="input-group-append">
              <button type="submit" class="btn btn-secondary">Redeem</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-8 order-md-1">
        <h4 class="mb-3">Billing address</h4>
        <form class="needs-validation" novalidate="">
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="firstName">First name</label>
              <input type="text" class="form-control" id="firstName" placeholder="" value="" required="">
            </div>
            <div class="col-md-6 mb-3">
              <label for="lastName">Last name</label>
              <input type="text" class="form-control" id="lastName" placeholder="" value="" required="">
            </div>
          </div>

          <!-- <div class="mb-3">
            <label for="username">Username</label>
              <input type="text" class="form-control" id="username" placeholder="Username" required="">
          </div> -->
          
          <div class="mb-3">
            <label for="email">Email <span class="text-muted">(Optional)</span></label>
            <input type="email" class="form-control" id="email" placeholder="you@example.com">
          </div>

          <div class="mb-3">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" placeholder="1234 Main St" required=""> 
          </div>

          <div class="mb-3">
            <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
            <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
          </div>

          <div class="row">
            <div class="col-md-5 mb-3">
              <label for="country" class="col-md-12 text-left pl-0">Country</label>
              <select class="custom-select d-block w-100 form-control" id="country" required="">
                <option value="">Choose...</option>
                <option>United States</option>
              </select>
              
            </div>
            <div class="col-md-4 mb-3">
              <label for="state" class="col-md-12 text-left pl-0">State</label>
              <select class="custom-select d-block w-100 form-control" id="state" required="">
                <option value="">Choose...</option>
                <option>California</option>
              </select>
              
            </div>
            <div class="col-md-3 mb-3">
              <label for="zip">Zip</label>
              <input type="text" class="form-control" id="zip" placeholder="" required="">
              
            </div>
          </div>
          <hr class="mb-4">
          <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="same-address">
            <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
          </div>
          <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="save-info">
            <label class="custom-control-label" for="save-info">Save this information for next time</label>
          </div>
          <hr class="mb-4">

          <h4 class="mb-3">Payment</h4>

          <div class="d-block my-3">
            <div class="custom-control custom-radio">
              <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked="" required="">
              <label class="custom-control-label" for="credit">Credit card</label>
            </div>
            <div class="custom-control custom-radio">
              <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required="">
              <label class="custom-control-label" for="debit">Debit card</label>
            </div>
            <div class="custom-control custom-radio">
              <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required="">
              <label class="custom-control-label" for="paypal">Paypal</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="cc-name">Name on card</label>
              <input type="text" class="form-control" id="cc-name" placeholder="" required="">
              <small class="text-muted">Full name as displayed on card</small>
              
            </div>
            <div class="col-md-6 mb-3">
              <label for="cc-number">Credit card number</label>
              <input type="text" class="form-control" id="cc-number" placeholder="" required="">
              
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 mb-3">
              <label for="cc-expiration">Expiration</label>
              <input type="text" class="form-control" id="cc-expiration" placeholder="" required="">
              
            </div>
            <div class="col-md-3 mb-3">
              <label for="cc-expiration">CVV</label>
              <input type="text" class="form-control" id="cc-cvv" placeholder="" required="">
              
            </div>
          </div>
          <hr class="mb-4">
          <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- Js added for input number -->
<script type="text/javascript">
  function spinner() {
  //  SPINNER
  $("#spinner").spinner();
  
  //  INPUT ONLY NUMBERS
  $('#spinner').keyup(function () { 
     this.value = this.value.replace(/[^0-9]/g,'');
  });
}
// INPUT NUMBER MAX LENGHT
function maxLengthCheck(object) {
  if (object.value.length > object.maxLength)
    object.value = object.value.slice(0, object.maxLength)
}
window.onload = spinner;
</script>
<!-- Js added for input number -->
<script type="text/javascript">
  class ImageViewer {
  constructor(selector) {
    this.selector = selector;
    $(this.secondaryImages).click(() => this.setMainImage(event));
    $(this.mainImage).click(() => this.showLightbox(event));
    $(this.lightboxClose).click(() => this.hideLightbox(event));
  }
  
  get secondaryImageSelector() {
    return '.secondary-image';
  }
  
  get mainImageSelector() {
    return '.main-image';
  }
  
  get lightboxImageSelector() {
    return '.lightbox';
  }
  
  get lightboxClose() {
    return '.lightbox-controls-close';
  }
  
  get secondaryImages() {
    var secondaryImages = $(this.selector).find(this.secondaryImageSelector).find('img')
    return secondaryImages;
  }
  
  get mainImage() {
    var mainImage = $(this.selector).find(this.mainImageSelector);
    return mainImage;
  }
  
  get lightboxImage() {
    var lightboxImage = $(this.lightboxImageSelector);
    return lightboxImage;
  }
  
  setLightboxImage(event){
    var src = this.getEventSrc(event);
    this.setSrc(this.lightboxImage, src);
  }
  
  setMainImage(event){
    var src = this.getEventSrc(event);
    this.setSrc(this.mainImage, src);
  }
  
  getSrc(node){
    var image = $(node).find('img');
  }
  
  setSrc(node, src){
    var image = $(node).find('img')[0];
    image.src = src;
  }
  
  getEventSrc(event){
    return event.target.src;
  }
  
  showLightbox(event){
    this.setLightboxImage(event);
    $(this.lightboxImageSelector).addClass('show');
  }
  
  hideLightbox(){
    $(this.lightboxImageSelector).removeClass('show');
  }
}

new ImageViewer('.image-viewer');
</script>
<style type="text/css">
  .pb-header-main .navbar-nav>li>a{color: #fff;}
  /*canvas#bg {
      background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
  }*/
</style>

</body>
</html>
