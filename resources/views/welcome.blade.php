<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<header class="pb-header-main">
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
                <img src="{{ asset('images/logo_main.png') }}"></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
            @include('nav.menu')
          </ul>
        </div>
        
    </nav>
    <div class="header_bottom section_arrow" id="header_bottom" style="border:1px red solid;display: none;">
        <nav class="navbar">
          <ul class="nav navbar-nav navbar-left">
            @include('nav.menu')
          </ul>
        </nav>
    </div>
</header>
<div class="pb-main-banner">
    <div class="container_circle">
        <div class="circle-box-rotate"></div>
    </div>
    <div class="circle-wrap circle-wrap1">
        <div class="badge-circle-wrap badge-circle-wrap1">
            <div class="badge-circle badge-circle1" id="badge-circle1">
              <h2>Self</h2>
            </div>
            <div class="badge-circle badge-circle1" id="badge-circle1-1">
              <h2>Reflection </h2>
            </div>
        </div>
        <div class="badge-circle-wrap badge-circle-wrap2">
            <div class="badge-circle badge-circle2" id="badge-circle2">
              <h2>Joyful</h2>
            </div>
            <div class="badge-circle badge-circle2" id="badge-circle2-1">
              <h2>Rhythms</h2>
            </div>
        </div>
        <div class="badge-circle-wrap badge-circle-wrap3">
            <div class="badge-circle badge-circle3" id="badge-circle3">
              <h2>Learning</h2>
            </div>
            <div class="badge-circle badge-circle3" id="badge-circle3-1">
              <h2>Journeys</h2>
            </div>
        </div>
        <div class="badge-circle-wrap badge-circle-wrap4">
            <div class="badge-circle badge-circle4" id="badge-circle4">
              <!-- <h2>Grounding </h2> -->
               <h3 id="grounding">G R O U N D I N G</h3>
            </div>
        </div>
        <div class="circle-box circle-box1">
            <div class="circle-box2">
                <div class="badge-circle-wrap badge-circle-wrap5">
                    <div class="badge-circle badge-circle5" id="badge-circle5">
                      <h3 id="spiritual">PHYSICAL</h3>
                    </div>
                </div>
                <div class="badge-circle-wrap badge-circle-wrap6">
                    <div class="badge-circle badge-circle6" id="badge-circle6">
                      <h2>Etheric</h2>
                    </div>
                </div>
                <div class="badge-circle-wrap badge-circle-wrap7">
                    <div class="badge-circle badge-circle7" id="badge-circle7">
                      <h2>Emotional</h2>
                    </div>
                </div>
                <div class="badge-circle-wrap badge-circle-wrap8">
                    <div class="badge-circle badge-circle8" id="badge-circle8">
                      <h2>Intellectual</h2>
                    </div>
                </div>
                <div class="badge-circle-wrap badge-circle-wrap9">
                    <div class="badge-circle badge-circle9" id="badge-circle9">
                      <h3 id="physical">SPIRITUAL</h3>
                    </div>
                </div>
                <div class="circle-box3 circle-box3-inner">
                    <div class="badge-circle-wrap badge-circle-wrap10">
                        <div class="badge-circle badge-circle10" id="badge-circle10">
                          <h2>WILLING<!-- Thinking --></h2>
                        </div>
                        <div class="badge-circle badge-circle11" id="badge-circle11">
                          <h2>Feeling</h2>
                        </div>
                        <div class="badge-circle badge-circle12" id="badge-circle12">
                          <h3 id="willing">THINKING<!-- WILLING --></h3>
                        </div>
                    </div>
                    <div class="circle-box4 circle-box4-inner">
                        <div class="view">
                          <div class="plane main">
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                            <div class="circle"></div>
                          </div>
                        </div>
                        <div class="badge-circle-wrap badge-circle-wrap13">
                            <div class="badge-circle badge-circle13" id="badge-circle13">
                              <h2>Being</h2>
                            </div>
                            <div class="badge-circle badge-circle14" id="badge-circle14">
                              <h2>Doing</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ground banner-title">
      <h3>Self Mastery . Transformation . Wellbeing .</h3>
      <!-- <h4></h4> -->
    </div>
    <div class="ground"></div>
    <canvas id="bg" width="800" height="800"></canvas>
    <!-- <div class="scolldown pw-purplecroll pw-scrollDown scolldown1">
        <figure><button type="button" scroll-data-id="#after-banner"><img data-src="images/arrow-2.png" alt="" class="lazy-loaded" data-lazy-type="image" src="{{ asset('images/arrow-2.png') }}"></button></figure>
    </div> -->
</div>
<!-- bottom navigation -->
<section id="after-banner" class="section section-intergal intergal-bg section_arrow" style="padding-top: 25px;padding-bottom: 25px;">
    <div class="container">
        <!-- <div>
          <h2 class="text-black wow slideInDown text-center mb-25" data-wow-duration="0.5s" data-wow-delay="0.3s">Explore the power of wellbeing!</h2>
        </div> -->
        <div class="flex-row-center" style="align-items: end;">
            <div class="col-sm-12 col-md-7 text-center">
                <h2 class="heading-color wow slideInDown text-left mb-25" data-wow-duration="0.5s" data-wow-delay="0.3s">Discover the Power of Self Mastery</h2>
                <div class="intergal-box">
                  <p class="wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">At Poorna Wellbeing we pride ourselves as experts in personal and organizational transformation through customized, immersive and creative experiences. We design and deliver integrative and unique programs, enabling individuals, organizations and community to lead fulfilling and impactful lives. We support individuals to live their personal and professional life with Holistic Wellbeing, Balance, Growth and Inspiration.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-5 text-center">
                <div class="intergal-img"><img src="{{ asset('images/logo_main.png') }}"></div>
            </div>
        </div>
    </div>
</section>
<!-- About -->
<section class="section section-about section-about-bg">
    <div class="container">
        <div class="row flex-row-center">
            <div class="col-sm-7 col-md-7  wow slideInLeft" data-wow-duration="0.6s" data-wow-delay="0.4s">
                <div class="about-text-box">
                    <h2 class="mb-25">Founder &amp; CEO – Deepa Mahesh</h2>
                    <p>Hi, I am Deepa and I hold the vibrational essence of Love, Power, Creativity, Oneness and Connectedness in the Core of my Being. I work passionately to manifest my core in my personal and professional life.
                    </p>
                    <p>My vision is to “Enhance Self Mastery with Wellbeing consciousness”. My endeavor is to serve the needs of my clients as I co-create the journey of transformation.</p>
                    <a href="#." class="btn btn-about">Read More</a>
                </div>
            </div>
            <div class="col-sm-5 col-md-5 wow slideInRight" data-wow-duration="0.6s" data-wow-delay="0.4s">
                <div class="about-profile">
                 <img src="{{ asset('images/ab-profile.png') }}">
                </div>
                <!-- <div class="about-circular-box-wrap"> -->
                 
                    <!-- <div class="circular-box circular-box1">
                        <img src="{{ asset('images/ab-profile.jpg') }}">
                    </div> -->
                    <!-- <div class="circular-box circular-box2">
                        <img src="{{ asset('images/ab-img2.jpg') }}">
                    </div>
                    <div class="circular-box circular-box3">
                        <img src="{{ asset('images/ab-img3.jpg') }}">
                    </div>
                    <div class="circular-box circular-box4">
                        <img src="{{ asset('images/ab-img4.jpg') }}">
                    </div> -->
                <!-- </div> -->
            </div>
        </div>
        <!-- <div class="row">
            <div class="scolldown pw-purplecroll pw-scrollDown scolldown1">
                <figure><button type="button" scroll-data-id="#section-aprroach"><img data-src="images/arrow-2.png" alt="" class="lazy-loaded" data-lazy-type="image" src="images/arrow-2.png"></button></figure>
            </div>
        </div> -->
    </div>
</section>
<section class="section section-aprroach section-aprroach-bg" id="section-aprroach">
    <div class="container-fluid">
        <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 wow slideInDown" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="heading-box text-center">
                    <h2>POORNA WELLBEING APPROACH</h2> 
                </div>
            </div>
        </div> -->
        <div class="row flex-row-center">
          <div class="approach-box-sec">
            <div class="content" style="padding: 0px;">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-xs-5 col-sm-5 col-md-5 wow slideInLeft badge" data-wow-duration="0.5s" data-wow-delay="0.3s">
                    <div class="approach-box-left">
                        <div class="approach-box-circle">
                            <div class="approach-box1"><p><span>1</span><br>Diagnosis</p></div>
                            <div class="approach-box2"><p><span>2</span><br>Design</p></div>
                            <div class="approach-box3"><p><span>3</span><br>Deliver</p></div>
                            <div class="approach-box4"><p><span>4</span><br>Measure</p></div>
                        </div>
                    </div>
                  </div>
                  <div class="col-xs-7 col-sm-7 col-md-7 approach-box-right approach-box-text">
                      <h2 class="mb-25">Our Approach</h2>
                      <p>We serve our client’s highest needs using a systematic and a holistic approach</p>
                      <ul>
                          <li>Diagnosis – Assess current and desired state</li>
                          <li>Design – Co-Create customized interventions</li>
                          <li>Delivery – Execute with focus and sensitivity</li>
                          <li>Measurement – Assess achievement of desired outcome</li>
                      </ul>
                  </div>
                  <!-- <h2>POORNA WELLBEING APPROACH</h2>
                  <p></p><h3><strong>Online Mental Health &amp; Wellbeing Services</strong></h3>
                  <p>From webinars to therapy, to support groups, resources and more. Our services are now online, offering the support needed while you stay home, and stay safe.</p>
                  <p><em><strong><span style="font-size: 25px;">We are here to help.</span></strong></em></p>
                  <p></p>
                  <a href="https://www.lighthousearabia.com/online-services/" class="btn btn-gold">View our Online Services</a> -->
                </div>
              </div>
            </div>
          </div>
            <!-- <div class="col-xs-12 col-sm-6 col-md-6 wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="approach-box-left">
                    <div class="approach-box-circle">
                        <div class="approach-box1"><p><span>1</span><br>Diagnosis</p></div>
                        <div class="approach-box2"><p><span>2</span><br>Design</p></div>
                        <div class="approach-box3"><p><span>3</span><br>Deliver</p></div>
                        <div class="approach-box4"><p><span>4</span><br>Measure</p></div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="col-xs-12 col-sm-6 col-md-6 wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.3s"> -->
              
                <!-- <div class="approach-box-right approach-box-text">
                    <h2>POORNA WELLBEING APPROACH</h2>
                    <p>We follow the Poorna (Holistic) Wellbeing approach to service client’s expressed and latent needs </p>
                    <ul>
                        <li>Diagnosis – Assess client’s current and desired state of Being (Individual, Group, Organization) </li>
                        <li>Design – Customized Intervention design  </li>
                        <li>Delivery – Execution with focus and sensitivity  </li>
                        <li>Measurement – Assess actualization of desired outcome</li>
                    </ul>
                </div> -->
            <!-- </div> -->
        </div>
        <!-- <div class="row">
            <div class="scolldown pw-purplecroll pw-scrollDown scolldown1">
                <figure><button type="button" scroll-data-id="#section-offering"><img data-src="images/arrow-2.png" alt="" class="lazy-loaded" data-lazy-type="image" src="images/arrow-2.png"></button></figure>
            </div>
        </div> -->
    </div>
</section>
<section class="section section-offering section-offering-bg" id="section-offering">
    <div class="container">
      <div class="row flex-row-center">
        <div class="col-xs-12 col-sm-12 col-md-12 wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.3s">
            <div class="offering-box-left text-center mb-25">
                <h2 class="mb-25">Our Offerings</h2>
                <p>We at Poorna Wellbeing want to nurture the burning desire of Self Mastery and holistic Wellbeing. Our offerings are integrative, deep, research based, outcome focused and experiential that enable clients to live holistic. Our interventions integrate methodologies like expressive arts, psychology, dance and movement, visual arts, theater, mindfulness, which activate both the right and left brain to bring about transformation.</p>
            </div>
        </div>
      </div>
        <div class="row flex-row-center" style="align-items: end;">
            <div class="col-xs-4 col-sm-4 col-md-4 wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="offering-box-left col-xs-11 col-sm-11 col-md-11 m-h-500">
                  <div class="offering-img1">
                   <img src="{{ asset('images/individual.jpg') }}">
                  </div>
                  <h2 class="text-center">Individual</h2>
                  <p>We offer Life and Leadership Coaching to individuals to connect within, transform self-limiting patterns and radiate their power in all areas of Life.</p>
                  <a href="#" class="btn btn-offer">Read More</a>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="offering-box-left col-xs-11 col-sm-11 col-md-11 m-h-500">
                  <div class="offering-img1">
                   <img src="{{ asset('images/offering-organization.png') }}">
                  </div>
                  <h2 class="text-center">Organization</h2>
                  <p>We enable organization and teams to ground and root into their desired culture and shoot and shine to achieve planned organizational objectives.</p>
                  <a href="#" class="btn btn-offer">Read More</a>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="offering-box-left col-xs-11 col-sm-11 col-md-11 m-h-500">
                  <div class="offering-img1">
                   <img src="{{ asset('images/community.jpg') }}">
                  </div>
                  <h2 class="text-center">Community</h2>
                  <p>We impact humanity by spreading awareness of Self Mastery and Holistic Wellbeing enabling them to live a life by choice.</p>
                  <a href="#" class="btn btn-offer">Read More</a>
                </div>
            </div>
            <!-- <div class="col-xs-12 col-sm-7 col-md-7 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="row text-center circular-row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <a href="#.">
                          <div class="offering-box offering-box1" id="offering-box1">
                            <h3>Integral <br> Wellbeing <br> Coaching</h3> 
                          </div>
                        </a>
                        <h4 id="offeringSubHead1">Individuals</h4>
                          <div class="offering-sec">
                          <p>Poorna Wellbeing takes prides in its customized offerings</p>
                          <a href="#." class="btn btn-offer">Read More</a>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <a href="#."><div class="offering-box offering-box2" id="offering-box2">
                            <h3>Group <br> Dynamic <br> Facilitation</h3>
                        </div></a>
                        <h4 id="offeringSubHead2">Groups</h4>
                        <div class="offering-sec" style="padding-top: 20px;">
                          <p>Poorna Wellbeing takes prides in its customized offerings</p>
                          <a href="#." class="btn btn-offer">Read More</a>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <a href="#."><div class="offering-box offering-box3" id="offering-box2">
                            <h3>Community <br> Wellbeing <br> Initiatives</h3>
                        </div></a>
                        <h4 id="offeringSubHead3">Community</h4>
                        <div class="offering-sec" style="padding-top: 14px;">
                          <p>Poorna Wellbeing takes prides in its customized offerings</p>
                          <a href="#." class="btn btn-offer">Read More</a>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- <div class="row">
            <div class="scolldown pw-purplecroll pw-scrollDown scolldown1">
                <figure><button type="button" scroll-data-id="#section-testimonials"><img data-src="images/arrow-2.png" alt="" class="lazy-loaded" data-lazy-type="image" src="images/arrow-2.png"></button></figure>
            </div>
        </div> -->
    </div>
</section>

@include('section.testimonials')

<section class="section section-contact-hm contact-hm-bg" id="section-contact-hm" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="hm-contact-box">
                    <div class="hm-contact-heading">
                        <h3>ENGAGE WITH POORNA WELLBEING </h3>
                        <p>Interested in enhancing your Holistic Wellbeing? Engage with  us …</p>
                    </div>
                    <ul>
                        <li><a href="#.">Apply for Internship </a></li>
                        <!-- <li><a href="#.">Apply for Internship </a></li> -->
                    </ul>
                    <p>We have exciting projects throughout the year. Apply for internships and enhance your professional profile. </p>
                    <ul>
                        <li><a href="#.">Volunteer Your Talents </a></li>
                        <li><a href="#.">Share your Wellbeing story </a></li>
                    </ul>
                    <p>If you are on your journey to wellbeing, inspire others by sharing your story. Send us your video and we will share it with the world. Lets create a Holistic Wellbeing revolution together!</p>
                    <!-- <form action="">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" id="mobile" placeholder="Mobile">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" placeholder="Massage"></textarea> 
                        </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                    </form> -->
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="fb-feed-box">
                    <h3>Inspire Yourself through Our Blogs </h3>
                    <div class="fb-feed">
                        <img src="images/fbfeed.jpg">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="scolldown pw-purplecroll pw-scrollDown scolldown1">
                <figure><button type="button" scroll-data-id="#section-upcoming-events"><img data-src="images/arrow-2.png" alt="" class="lazy-loaded" data-lazy-type="image" src="images/arrow-2.png"></button></figure>
            </div>
        </div>
    </div>
</section>
<section class="section section-upcoming-events upcoming-events-bg" id="section-upcoming-events" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 wow slideInDown" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="hm-contact-heading text-center">
                    <h2>UPCOMING EVENTS </h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="upcoming-box wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                    <img src="images/calender.png">
                    <a href="#." class="btn btn-register">Register</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.4s">
                <div class="upcoming-box">
                    <h4 class="evt-title">Ania CIrcle</h4>
                    <p class="date">25th April 2020, Saturday <br>
                    9.00 AM- 5.00 PM <br>
                    Online event
                    </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p>
                    <a href="#." class="btn btn-register">Register</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <div class="upcoming-box">
                    <h4 class="evt-title">Envisioning my future – Youth edition</h4>
                    <p class="date">1st June 2020, Monday <br>
                        9.00 AM- 5.00 PM <br>
                        Maps link – Poorna wellbeing center 
                    </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. </p>
                    <a href="#." class="btn btn-register">Register</a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('layouts.footer')
<!-- Modal -->
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModalLabel" aria-hidden="true">
  <div class="modal-dialog top-100" role="document">
    <div class="modal-content login-container">
        <button type="button" class="log-reg-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="box"></div>
          <div class="container-forms">
            <div class="container-info">
              <div class="info-item">
                <div class="table">
                  <div class="table-cell">
                    <p>
                      Have an account?
                    </p>
                    <div class="col-md-12 text-center">
                      <div class="btn">
                        Log in
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="info-item">
                <div class="table">
                  <div class="table-cell">
                    <p>
                      Don't have an account? 
                    </p>
                    <div class="col-md-12 text-center">
                      <div class="btn">
                        Sign up
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-form">
              <div class="form-item log-in">
                <div class="table">
                  <form class="table-cell">
                    <input name="Username" placeholder="Username" type="text" /><input name="Password" placeholder="Password" type="Password" />
                    <div class="col-md-12 text-center">
                      <div class="btn">
                        Log in
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="form-item sign-up">
                <div class="table">
                  <form class="table-cell">
                    <input name="email" placeholder="Email" type="text" /><input name="fullName" placeholder="Full Name" type="text" /><input name="Username" placeholder="Username" type="text" /><input name="Password" placeholder="Password" type="Password" />
                    <div class="col-md-12 text-center">
                      <div class="btn">
                        Sign up
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</div>

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.lettering.js"></script>
<script src="js/circletype.min.js"></script>
<script type="text/javascript">

    jQuery(function() {
        jQuery(".circle-wrap h2").lettering();
    });
    new CircleType(document.getElementById('grounding')).dir(-1).radius(185);
    new CircleType(document.getElementById('physical')).dir(-1).radius(185);
    new CircleType(document.getElementById('willing')).dir(-1).radius(140);
    new CircleType(document.getElementById('spiritual')).dir(-1).radius(185);
    new CircleType(document.getElementById('offeringSubHead1')).dir(-1).radius(120);
    new CircleType(document.getElementById('offeringSubHead2')).dir(-1).radius(120);
    new CircleType(document.getElementById('offeringSubHead3')).dir(-1).radius(120);
  

    //new CircleType(document.getElementById('badge-circle1')).radius(100);
    /*var demo5 = new CircleType(document.getElementById('badge-circle1')).radius(180);
    jQuery(demo5.element).fitText();*/
</script>

<script type='text/javascript'>//<![CDATA[ 
    window.onload=function(){<!-- ww w.  jav a 2  s . c  om-->
    function setSunBg(canvasId, n) {
        var bgCanvas = document.getElementById(canvasId);
        var width = parseInt(bgCanvas.getAttribute('width'), 10);
        var height = parseInt(bgCanvas.getAttribute('height'), 10);
        var cx = width / 2;
        var cy = height / 2;
        if (bgCanvas && bgCanvas.getContext) {
            var context = bgCanvas.getContext('2d');
            if (context) {
                var grd = context.createRadialGradient(cx, cy, 0, cx, cy, cy);
                // grd.addColorStop(0, '#ffe');
                // grd.addColorStop(0.3, '#ff8');
                // grd.addColorStop(0.4, '#ff5');
                grd.addColorStop(0, 'rgba(255, 255, 255, 0.80)');
                grd.addColorStop(0, 'rgba(255, 255, 255, 0.80)');
                grd.addColorStop(0.3, '#fff');
                //grd.addColorStop(0.4, '#ff0');
                grd.addColorStop(1, 'rgba(255, 0, 0, 0.0)');
                context.fillStyle = grd;
                context.fillRect(0, 0, width, height);
                for (i = 1; i <= n; i += 2) {
                    context.beginPath();
                    context.moveTo(cx, cy)
                    context.arc(cx, cy, Math.sqrt(Math.pow(cx, 2) + Math.pow(cy, 2)), 2 / n * Math.PI * (i - 1), 2 / n * Math.PI * i, false);
                    context.lineTo(cx, cy)
                    context.fillStyle = 'rgba(255,121,0,0.3)';
                    context.fill();
                    context.closePath();
                }
            }
        }
    }
    setSunBg('bg', 60);
    }//]]>  
</script>
<script type="text/javascript">
    num = $('.header_bottom').offset().top + 80;
    $(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
        wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
          wow.init();
    });
</script>
<!-- scroll dwon -->
<script type="text/javascript">
    function headerHeightCalc(){
      var getNiknkHeader = jQuery('#header_bottom').outerHeight();
      var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
      var calcHeaderHei = getNiknkHeader + getwpAdminbar;
      return calcHeaderHei;
    }
    $('.pw-scrollDown button').click(function(){
      var sectionOffset = headerHeightCalc();
      var getHrefVal = jQuery(this).attr('scroll-data-id');
      if(getHrefVal == '#header_bottom'){
        $('html, body').animate({
            scrollTop: jQuery(getHrefVal).offset().top - 0},
            'slow');
      }else{
        $('html, body').animate({
            scrollTop: jQuery(getHrefVal).offset().top - 78},
            'slow');
      }
      
    });
</script>
<style type="text/css">
    .pb-header-main .navbar-nav>li>a{color: #fff;}
</style>
<script type="text/javascript">
$(".info-item .btn").click(function(){
  $(".login-container").toggleClass("log-in");
});
$(".container-form .btn").click(function(){
  $(".login-container").addClass("active");
});
</script>
</body>
</html>
