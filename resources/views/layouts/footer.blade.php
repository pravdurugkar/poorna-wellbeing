<footer class="footer section footer-bg">
    <div class="footer-top section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <ul class="footer-social-link">
                        <li>
                          <a href="#.">
                          <div class="social-box">
                            <img src="{{ asset('images/facebook.png') }}">
                          </div>
                          </a>
                        </li>
                        <li>
                          <a href="#.">
                          <div class="social-box">
                            <img src="{{ asset('images/instagram.png') }}">
                          </div>
                          </a>
                        </li>
                        <li>
                          <a href="#.">
                          <div class="social-box">
                            <img src="{{ asset('images/linkedin.png') }}">
                          </div>
                          </a>
                        </li>
                        <li>
                          <a href="#.">
                          <div class="social-box">
                            <img src="{{ asset('images/youtube.png') }}">
                          </div>
                          </a>
                        </li>
                    </ul>
                </div>
                <!-- <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="subscribe-newslatter">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" placeholder="Subscribe to our newsletter">
                                <button type="submit" name="submit" class="btn btn-newslatter">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="section footer-logo">
        <img src="/images/logo_main.png">
    </div>
    <div class="footer-bottom section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-left">
                  <div class="footer-contact">
                        <ul class="contact-items" style="margin: 10px 0px 0px 9px;">
                          <li><a href="emailto:deepa@domain.com"><i class="fa fa-mobile" aria-hidden="true"></i> <span>+91 96860 51005</span></a></li>
                          <li><a href="emailto:deepa@domain.com"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>deepa@poornawellbeing.com</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-right">
                    <div class="footer-contact" style="margin: 10px 0px 0px 9px;">
                      <div style="margin:0px 0px 0px 30px;font-size: 18px;display: flex;">
                         <i class="fa fa-map-marker" aria-hidden="true" style="padding-right: 10px;"></i> <div> Poorna Wellbeing <br>
                        Bengaluru - 560038</div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>