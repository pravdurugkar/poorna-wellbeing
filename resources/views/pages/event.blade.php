<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <style type="text/css">
      .pramotional-box .download-poster {
        display: none;
      }
      .pramotional-box:hover .download-poster {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        text-align: center;
        color: white;
        font-size: 30px;
        padding: 5px;
      }
    </style>
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-event_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="event-banner_head">
					<p>Poorna Wellbeing Event Management</p>
					<h1>{{ $event->title }}</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-event_listing">
	<div class="container">
  <div class="product-post event-post pb-5">
		<div class="row">
			<?php
				//foreach ($events as $event) { ?>

					<div class="col-lg-6 col-md-6 col-sm-12">
		        <!-- <div class="product-teaser event-teaser"> -->
						<div class="event-image col-lg-11 col-md-11 col-sm-12 pl-0">
					    <div class="field field-field-imagefield-item pramotional-box">
					    		<img src="{{ asset('images/event/event-img-09.jpg') }}" width="480" height="480" alt="Event">
                  <!-- <iframe id="video" width="480" height="480"  src="//www.youtube.com/embed/9B7te184ZpQ?rel=0" frameborder="0" allowfullscreen></iframe> -->
                  <a download="event-{{$event->permalink}}-poster.jpg" class="download-poster" href="{{ asset('images/event/event-img-09.jpg') }}" title="Download Poster">
                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                  </a>
					    </div>
						</div>
          </div>
          <div class="col-md-6 product-content-wrap event-content-wrap">
            <div class="event-category">
              <div class="field field-field-event-category field-label-hidden field-item">
                <a href="/pana/event-category/seminar" hreflang="en">Seminar</a>
              </div>
            </div>
            <div class="event-title row">
              <div class="product-title col-lg-10">
                <div class="product--variation-field--variation_title__20 field field-title field-label-hidden field-item">{{ $event->title }}</div>
              </div> 
              <div class="product-price col-lg-2 text-right">
                <div class="product--variation-field--variation_price__20 field field-price field-label-hidden field-item">₹{{ $event->price }}</div>
              </div>
            </div>
            <div class="event-meta-wrap">
              <?php
                $start_date = strtotime($event->start_date_time);
                $end_date = strtotime($event->end_date_time);

                $start_date_df = new DateTime($event->start_date_time);
                $since_start = $start_date_df->diff( new DateTime($event->end_date_time) );

                
              ?>
              <div class="row event-duration">
                <div class="col-md-3 event-date">
                  <h6 class="event-meta-tile">Date</h6>
                  {{ date('M jS, Y', $start_date) }}
                </div>
                <div class="col-md-3 event-time">
                  <h6 class="event-meta-tile">Time</h6>
                  {{ date('h:i a', $start_date) }} - {{ date('h:i a', $end_date) }}
                </div>

                <div class="col-md-3 event-duration">
                  <h6 class="event-meta-tile">Duration</h6>
                  <?php
                    if ($since_start->days)
                      echo $since_start->days.' days total, ';

                    if ($since_start->y)
                      echo $since_start->y.' yr, ';

                    if ($since_start->m)
                      echo $since_start->m.' months, ';

                    if ($since_start->d)
                      echo $since_start->d.' days, ';

                    if ($since_start->h)
                      echo $since_start->h.' Hrs, ';

                    if ($since_start->i)
                    echo $since_start->i.' mins ';

                    // if ($since_start->s)
                      // echo $since_start->s.' seconds';
                  ?>
                </div>
              
                @isset($event->location)
                <div class="col-md-3 event-venue-wrap">
                  <h6 class="event-meta-tile">Venue</h6>
                  <div class="event-venue">{{ $event->venue }}<span>,</span></div>
                  <div class="event-location">
                    <div class="field field-field-event-location field-label-hidden field-item">
                      <a href="/pana/event-location/new-york" hreflang="en">{{ $event->location }}</a>
                    </div>
                  </div>
                </div>
                @endisset
              </div>
            </div>
              <div class="product-body mb-25">
                <div class="field field-body field-label-hidden field-item">
                  <!-- <p>Certain be ye amiable by exposed so celebrated estimating excellence do furnished do otherwise conveying attempted. These are just some of the numerous reasons of choosing and sticking with us alone.</p> -->
                  <p>{{ $event->description }}</p>
                </div>
              </div>
              <div class="quantity">
                <form method="get" action="/event/{{$event->permalink}}/booking">
                  <input name="quantity" type="number" min="1" max="10" step="1" value="1">
                  <div class="quantity-nav">
                    <div class="quantity-button quantity-up">+</div>
                    <div class="quantity-button quantity-down">-</div>
                    <button class="button js-form-submit form-submit ticket-btn" type="submit" id="ticket-submit" name="op">Get Ticket</button>
                  </div>
                </form>
              </div>
        </div>

    	</div>
    </div>
    <hr>
    <h1 class="text-center">Event Gallery</h1>
    <ul class="gallery_box">
      <li>
        <a href="#0">
          <img src="https://picsum.photos/600/700/?random">
        </a>
      </li>
        <li>
        <a href="#0"><img src="https://picsum.photos/600/701/?random">
        </a>
      </li>
        <li>
        <a href="#0"><img src="https://picsum.photos/600/702/?random">
        </a>
      </li>
          <li>
        <a href="#0"><img src="https://picsum.photos/600/703/?random">
        </a>
      </li>
          <li style="    position: relative;
        top: -134px;">
        <a href="#0"><img src="https://picsum.photos/600/704/?random">
        </a>
      </li>
          <li>
        <a href="#0"><img src="https://picsum.photos/600/705/?random">
        </a>
      </li>
    </ul>
	</div>
</section>

<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
	num = $('.header_bottom').offset().top + 80;
	$(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
	    wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
	      wow.init();
	});
</script>
<!-- scroll dwon -->
<script type="text/javascript">
	function headerHeightCalc(){
	  var getNiknkHeader = jQuery('#header_bottom').outerHeight();
	  var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
	  var calcHeaderHei = getNiknkHeader + getwpAdminbar;
	  return calcHeaderHei;
	}
	$('.pw-scrollDown button').click(function(){
	  var sectionOffset = headerHeightCalc();
	  var getHrefVal = jQuery(this).attr('scroll-data-id');
	  if(getHrefVal == '#header_bottom'){
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 0},
	        'slow');
	  }else{
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 78},
	        'slow');
	  }
	  
	});
</script>

<script type="text/javascript">
	
	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});
	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
	.pb-header-main .navbar-nav>li>a{color: #fff;}
	/*canvas#bg {
	    background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
	}*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
