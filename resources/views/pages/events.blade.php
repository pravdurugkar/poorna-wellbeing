<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Poorna Wellbeing</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  	<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-event_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="event-banner_head">
					<p>Poorna Wellbeing Event Management</p>
					<h1>Events Listing</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-event_listing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="event-list_head">
					<h2>Upcoming Events</h2>
					<p>These are just some of the numerous reasons of choose us.</p>
				</div>
			</div>
		</div>
		<div class="row grid-wrapper">
			<?php
				foreach ($events as $event) { ?>
					
					<div class="col-12 col-sm-12 col-md-6 col-lg-3 grid-item">
		        		<div class="product-teaser event-teaser">
							<div class="event-image">
							    <div class="field field-field-imagefield-item">
							    	<a href="#" >
							    		<img src="{{ asset('images/event/event-img-09.jpg') }}" width="480" height="480" alt="Event">
							    	</a>
							    </div>
							</div>
							<div class="event-content-wrap">
								<div class="event-content-container">
									<div class="event-content">
										<div class="event-title">
											<a href="/event/{{ $event->permalink }}">
									            <div class="field field-title field-label-hidden field-item">{{ $event->title }}</div>
									      	</a>
											<div class="event-price">
					            				<div class="product--variation-field--variation_price__20 field field-price field-label-hidden field-item">₹{{ $event->price }}</div>
					      					</div>
										</div>
										<div class="event-meta">
											<div class="event-date-wrap">
												<div class="event-date">
												<?php
													$start_date = strtotime($event->start_date_time);
												?>
												<span class="event-date-day">{{ date('jS', $start_date) }}</span>
												<span class="event-date-month">{{ date('M', $start_date) }}</span>
												<span class="event-date-year">{{ date('Y', $start_date) }}</span>
												</div>
											</div>
											<div class="event-time">
												<span class="fa fa-clock-o"></span> {{ date('h:i a', $start_date) }}
											</div>
											@isset($event->location)
											<div class="event-venue-wrap">
												<span class="fa fa-map-marker"></span>
												<div class="event-venue">{{ $event->venue }}<span>,</span></div>
												<div class="event-location">
						            				<div class="field field-field-event-location field-label-hidden field-item">
						            					<a href="#">{{ $event->location }}</a>
						            				</div>
						      					</div>
											</div>
											@endisset
										</div>
									</div>
								</div>
							</div>
							<div class="event-button">
								<a class="button" href="/event/{{$event->permalink}}/booking">Register</a>
							</div>
						</div>
					</div>

				<?php }
			?>
    	</div>
	</div>
</section>

<!--counter section-->
<section class="section section-counter section-counterBg">
	<div class="container">
		 <div class="row">
			<div class="col-md-3 col-sm-6 fact text-light">
				<h1 class="count-number counter">624</h1>
				<h5 class="fact-title">Members</h5>
			</div> <!-- fact-1 -->
		      <div class="col-md-3 col-sm-6 fact text-light">
				<h1 class="count-number counter">3635</h1>
			        <h5 class="fact-title">Participants</h5>
		      </div> <!-- fact-2 -->
		      <div class="col-md-3 col-sm-6 fact text-light">
				<h1 class="count-number counter">268</h1>
		        	<h5 class="fact-title">Sponsors</h5>
		      </div> <!-- fact-3 -->
		      <div class="col-md-3 col-sm-6 fact text-light">
			<h1 class="count-number counter">103</h1>
		        <h5 class="fact-title">Locations</h5>
		      </div> <!-- fact-4 -->
		</div>
	</div>
</section>
<!--counter end-->

<!---FAQ-->
@include('section.faq')
<!--end FAQ--->

<!--blog section-->
<section class="section section-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="event-list_head">
					<h2>Latest News</h2>
					<p>Read latest news and important updates.</p>
				</div>
			</div>
		</div>
		<div class="row grid-wrapper">
        	<div class="col-12 col-sm-12 col-md-4 col-lg-4 grid-item">
				<div class="blog-post-teaser post-row">
  					<div class="post-thumb">
						<div class="post-image"> 
							<a href="#" >
								<img src="{{ asset('images/blog/blog-img-07.jpg') }}" width="640" height="480">
							</a>
						</div>
						<div class="post-content-wrap">
							<div class="content-wrap">
								<div class="post-date">
									<span class="post-date-day">7</span>
									<span class="post-date-month">Dec</span>
								</div>
								<div class="post-meta-wrap">
									<div class="post-meta">
									<div class="post-meta-item post-category">
										<i class="fa fa-folder-o"></i> 
            							<div class="field field-field-category field-label-hidden field-item">
            								<a href="#" >Business</a>
            							</div>
      								</div>
									<div class="post-meta-item post-comment">
										<i class="fa fa-comments-o"></i> 6 comments
									</div>
								</div>
								<div class="post-title-wrap">
									<h5 class="post-title">
										<a href="#">
											<span>10 Inventions That Keeps You Thinking</span>
										</a>
									</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="post-body">
					<div class="post-body-content">
            			<div class="field field-body field-label-hidden field-item">Impossible considered invitation instrument saw celebrated. Rest and must set kind next many near nay exquisite continued.</div>
      				</div>
					<a class="read-more" href="#">
						<span class="read-more-text">Read More</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-4 col-lg-4 grid-item">
			<div class="blog-post-teaser post-row">
 				<div class="post-thumb">
					<div class="post-image">
						<a href="#" >
							<img src="{{ asset('images/blog/blog-img-02.jpg') }}" width="640" height="480">	
						</a>
					</div>
					<div class="post-content-wrap">
						<div class="content-wrap">
							<div class="post-date">
								<span class="post-date-day">7</span>
								<span class="post-date-month">Dec</span>
							</div>
							<div class="post-meta-wrap">
								<div class="post-meta">
									<div class="post-meta-item post-category">
										<i class="fa fa-folder-o"></i> 
            							<div class="field field-field-category field-label-hidden field-item">
            								<a href="#" >Business</a>
            							</div>
      								</div>
									<div class="post-meta-item post-comment">
										<i class="fa fa-comments-o"></i> 6 comments
									</div>
								</div>
								<div class="post-title-wrap">
									<h5 class="post-title">
										<a href="#">
											<span>10 Inventions That Keeps You Thinking</span>
										</a>
									</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="post-body">
					<div class="post-body-content">
            			<div class="field field-body field-label-hidden field-item">Impossible considered invitation instrument saw celebrated. Rest and must set kind next many near nay exquisite continued.</div>
      				</div>
					<a class="read-more" href="#">
						<span class="read-more-text">Read More</span>
					</a>
				</div>
			</div>
		</div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-4 grid-item">
			<div class="blog-post-teaser post-row">
  				<div class="post-thumb">
					<div class="post-image">
						<a href="#" hreflang="en">
							<img src="{{ asset('images/blog/blog-img-04.jpg') }}" width="640" height="480">
						</a>
					</div>
					<div class="post-content-wrap">
						<div class="content-wrap">
							<div class="post-date">
								<span class="post-date-day">28</span>
								<span class="post-date-month">Nov</span>
							</div>
							<div class="post-meta-wrap">
								<div class="post-meta">
									<div class="post-meta-item post-category">
										<i class="fa fa-folder-o"></i> 
            							<div class="field field-field-category field-label-hidden field-item">
            								<a href="#" hreflang="en">Design</a>
            							</div>
      								</div>
									<div class="post-meta-item post-comment">
										<i class="fa fa-comments-o"></i> 0 comments
									</div>
								</div>
								<div class="post-title-wrap">
									<h5 class="post-title">
										<a href="#">
											<span>Ideas That Really makes a Difference</span>
										</a>
									</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="post-body">
					<div class="post-body-content">
            			<div class="field field-body field-label-hidden field-item">Windows talking painted pasture yet its express parties use. Sure last upon same next of believed or diverted no rejoiced.</div>
      				</div>
					<a class="read-more" href="#">
						<span class="read-more-text">Read More</span>
					</a>
				</div>
			</div>
		</div>
    </div>
</div>
</section>
<!--end blog section-->
<!--testinomials-->
@include('section.testimonials')
<!--end-->

<!-- Footer -->
@include('layouts.footer')
<!-- END -->


<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
	num = $('.header_bottom').offset().top + 80;
	$(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
	    wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
	      wow.init();
	});
</script>
<!-- scroll dwon -->
<script type="text/javascript">
	function headerHeightCalc(){
	  var getNiknkHeader = jQuery('#header_bottom').outerHeight();
	  var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
	  var calcHeaderHei = getNiknkHeader + getwpAdminbar;
	  return calcHeaderHei;
	}
	$('.pw-scrollDown button').click(function(){
	  var sectionOffset = headerHeightCalc();
	  var getHrefVal = jQuery(this).attr('scroll-data-id');
	  if(getHrefVal == '#header_bottom'){
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 0},
	        'slow');
	  }else{
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 78},
	        'slow');
	  }
	  
	});
</script>

<script type="text/javascript">
	
	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});
	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
	.pb-header-main .navbar-nav>li>a{color: #fff;}
	/*canvas#bg {
	    background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
	}*/
</style>
</body>
</html>
