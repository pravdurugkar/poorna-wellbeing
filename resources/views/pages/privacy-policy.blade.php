<!DOCTYPE html>
<html lang="en">
<head>
  <title>Privacy Policy | Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-event_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="event-banner_head">
					<p>Poorna Wellbeing Event Management</p>
					<h1>Privacy Policy</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-terms">
	<div class="container">
    <div class="row">
      <p>Poorna Wellbeing will be the “owner” of customer’s data and all the information concerning the use of their personal data is summarised in the privacy document, in accordance with the Protection of Personal Information legislation. Customers must, therefore, accept such a policy before proceeding with any purchases, with a particular focus on the following areas:</p>
      <div class="clearfix"></div>

      <p><strong>1. Purposes of data processing.</strong><br>
      Commercial, promotional and/or sales purposes to identify potential customers for each specific product or service (eg, statistics, customer satisfaction feedback, preliminary gathering of information aimed at reaching an agreement or provision of a service, etc.); Customer satisfaction research (eg research on the quality of services); Fulfilment of obligations under the law, regulations or legislation.</p>

      <p><strong>2. Data communication.</strong><br>
      The personal data collected by Poorna Wellbeing will be communicated and/or processed by people we trust, who, in pursuit of the above objectives, carry out operational, technical and organizational tasks. The personal data provided by Customers are used only to perform the job or related services; the data are not disclosed to third parties unless such disclosure is required by law or is directly necessary for the fulfilment of requirements.</p>

      <p>All the details of the credit cards processed by email/ telephone/ fax are processed by the Paytm secure server and immediately cancelled after the transaction.</p>
      <div class="clearfix"></div>

      <p><strong>3. Rights of interested parties.</strong><br>
      It is fully established that the customer has the option to exercise specific rights. The Customer may obtain: confirmation or otherwise of the existence of data concerning them, even if not yet recorded.</p>

      <p>Information about the origin of personal data, the purpose and methods of processing it, and the basis of processing in cases where this is done using electronic instruments.</p>

      <p>Indication of the identity of the Owner and the Person Responsible, and the persons or categories of persons to whom the personal data may be communicated or who have access to them.</p>

      <p>The cancellation, transformation into anonymous form or blocking of data processed in violation of the law, as well as updating, rectification or, where interested, integration of data unless it is im-possible or involves the use of disproportionate means. The Customer may also object, for legitimate reasons, to the processing of personal data about them, even if it relates to the purpose of the data collection. The Customer can also object to the processing of personal data for sending advertising material or otherwise, aimed at carrying out market research and business communications.</p>
      <div class="clearfix"></div>

      <p><strong>4. Applicable Law and Competent Jurisdiction</strong><br>
      Indian law has regulated information within this sales contract. District courts of India will handle any disputes concerning the validity, interpretation or execution of this contact.</p>
      <div class="clearfix"></div>

      <p><strong>5. Modifications</strong><br>
      Poorna Wellbeing have the right to edit this document without notice at any time, and as a result will be effective from the date of its publication at <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a></p>
      <div class="clearfix"></div>

      <p><strong>6. Complaints</strong><br>
      Customers wishing to make a complaint must address their issues at Poorna Wellbeing registered office in India. <br>
      <address>
        <strong><em>Poorna Wellbeing, <br>
          Room no. 14, Montfort Spirituality Centre, <br>
          184, Old Madras Road, Indira Nagar, <br>
          Near Vivekananda Metro Station, <br>
          Bengaluru - 560038</em></strong>
        </address>
      </p>
      <div class="clearfix"></div>

      <p><strong>7. Additional Information</strong><br>
      All communication’s and contracts will be done so in English.</p>
                              
    </div>
	</div>
</section>

<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
	num = $('.header_bottom').offset().top + 80;
	$(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
	    wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
	      wow.init();
	});
</script>
<!-- scroll dwon -->
<script type="text/javascript">
	function headerHeightCalc(){
	  var getNiknkHeader = jQuery('#header_bottom').outerHeight();
	  var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
	  var calcHeaderHei = getNiknkHeader + getwpAdminbar;
	  return calcHeaderHei;
	}
	$('.pw-scrollDown button').click(function(){
	  var sectionOffset = headerHeightCalc();
	  var getHrefVal = jQuery(this).attr('scroll-data-id');
	  if(getHrefVal == '#header_bottom'){
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 0},
	        'slow');
	  }else{
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 78},
	        'slow');
	  }
	  
	});
</script>

<script type="text/javascript">
	
	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});
	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
	.pb-header-main .navbar-nav>li>a{color: #fff;}
	/*canvas#bg {
	    background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
	}*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
