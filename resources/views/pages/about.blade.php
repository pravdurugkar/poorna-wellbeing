<!DOCTYPE html>
<html lang="en">
<head>
  <title>About Us | Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
  @include('nav.menu')
</div>
<section class="section-event_bg" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="event-banner_head">
          <p>Poorna Wellbeing Event Management</p>
          <h1>Terms & Conditions</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg section-about-bg" id="section-our-story">
  <div class="container-fluid">
    <div class="row flex-row-center">
      <div class="approach-box-sec about-us-box">
        <div class="content" style="padding: 0px;">
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;">Our Story</h2>
            <div>
              <div class="col-xs-7 col-sm-7 col-md-7 approach-box-right approach-box-text about-us-text">
                  <p>Poorna Wellbeing is an organisation that strives to spread the consciousness of holistic wellbeing, balance and transformation. We believe that all human beings are radiant in their core. We also believe that organisations and teams always seek growth and transformation. </p>
                  <ul>
                      <li>Enable individuals to experience themselves as powerful and radiant and take life’s actions accordingly</li>
                      <li>Enable teams to connect to their core strengths, capacities and achieve their highest goals</li>
                      <li>Enable organisations to see their potential and work to manifest them</li>
                  </ul>
              </div>
              <div class="col-xs-5 col-sm-5 col-md-5 wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="our-story-video">
                  <iframe width="100%" height="250" src="https://www.youtube.com/embed/yps7bA7hiH8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="section-our-logo" class="section section-intergal intergal-bg section_arrow" style="padding-top: 65px;padding-bottom: 65px;">
  <div class="container">
    <div class="flex-row-center" style="align-items: end;">
        <div class="col-sm-12 col-md-7 text-center">
          <h2 class="heading-color wow slideInDown text-left mb-25 mt-0" data-wow-duration="0.5s" data-wow-delay="0.3s">Our Logo and the meaning it holds</h2>
            <div class="intergal-box">
              <p class="wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">Our logo is a symbol of what we hold very dear to us. The two key symbols represented in our logo are a Circle and a Tree.</p>
              <p class="wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">Tree is a metaphor for Grounding and Rooting and from there Shooting and Shining.  This universal symbol is the process we facilitate in our client systems.</p>
              <p class="wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">The Circle is a metaphor for wholeness and balance. We view the client system from a wholeness perspective and partner with them to thrive!</p>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 text-center">
            <div class="intergal-img"><img src="{{ asset('images/logo_main.png') }}"></div>
        </div>
    </div>
  </div>
</section>
<section class="section section-about section-about-bg" id="section-our-logo">
  <div class="container">
    <div class="row flex-row-center">
      <div class="col-sm-12 col-md-12">
        <div class="about-text-box">
          <h2 class="mb-25">Our Values</h2>
          <div class="row">
            <div class="col-md-3">
              <div class="values-circle circle-1 wow slideInLeft" data-wow-duration="0.6s" data-wow-delay="0.4s">
                <h3>EMPATHY</h3>
                <p>We listen deeply to ourself and our clients expressed and latent needs</p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="values-circle circle-2 wow slideInLeft" data-wow-duration="0.6s" data-wow-delay="0.4s">
                <h3>CREATIVITY</h3>
                <p>We create well researched & unique interventions </p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="values-circle circle-3 wow slideInRight" data-wow-duration="0.6s" data-wow-delay="0.4s">
                <h3>BALANCE</h3>
                <p>We embody and offer a balanced perspective in the design, methodologies and delivery of our offerings </p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="values-circle circle-4 wow slideInRight" data-wow-duration="0.6s" data-wow-delay="0.4s">
                <h3>AUTHENTICITY</h3>
                <p>We stand for honesty and consistency in our offerings, conversations and commitments.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="section-our-aspiration" class="section section-intergal intergal-bg section_arrow" style="padding-top: 65px;padding-bottom: 65px;">
  <div class="container">
    <div class="flex-row-center" style="align-items: end;">
      <div class="col-sm-12 col-md-7 text-center">
        <h2 class="heading-color wow slideInDown text-left mb-25 mt-0" data-wow-duration="0.5s" data-wow-delay="0.3s">Our Aspiration</h2>
        <div class="intergal-box">
          <p class="wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">We exist to serve the Wellbeing needs of Individuals, Organisations and Community.</p>
          <ul class="text-left our-aspiration-text">
            <li>We offer possibility to live conscious and joyous!</li>
            <li>We offer tools to take charge of yourself, team and organisation</li>
            <li>We offer spaces to engage in self-reflection, continuous learning and self-inquiry</li>
            <li>We offer practices to sustain self-care rhythmically</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-12 col-md-5 text-center">
        <div class="our-aspiration"><img src="{{ asset('images/our-Aspiration.png') }}"></div>
      </div>
    </div>
  </div>
</section>
<section class="section section-about section-about-bg">
  <div class="container">
    <div class="about-text-box">
      <h2>About the Founder & CEO</h2>
      <h2 class="mb-25">Deepa Mahesh</h2>
    </div>
    <div class="row">
      <div class="col-md-9 wow slideInLeft" data-wow-duration="0.6s" data-wow-delay="0.4s">
        <div class="about-text-box">
          <p>Hi, I am Deepa and in my core being, I hold the vibrational essence of Power, Love, Oneness, Creativity, Service and Connectedness.
          </p>
          <p>Born in Mumbai, India, I have had an artistic and spiritual upbringing. My post-graduation in Human Resources opened up another dimension close to my heart - the world of connecting with human sensitivities. During my HR stint of over 15 years, I used creative approaches for culture building, learning & development, employee engagement and young talent management in alignment with the strategic goals of the organization.</p>
          <p>My approach in working with individuals or groups was always based on deep listening and sensing the need, co-creating a personalized journey towards the goal and finally sustaining it for “transformation” to occur.</p>
        </div>
      </div>
      <div class="col-sm-3 col-md-3 wow slideInRight" data-wow-duration="0.6s" data-wow-delay="0.4s">
        <div class="about-profile">
         <img src="{{ asset('images/about-founder-deepa-mahesh.png') }}">
        </div>
      </div>
      <div class="col-md-12 wow slideInLeft" data-wow-duration="0.6s" data-wow-delay="0.4s">
        <div class="about-text-box">
          <p>Personal growth has always been my most important pursuit. That propelled me to quit my corporate role and re-enter the world of Arts as an Expressive Arts Therapist and workshop facilitator. I committed to intense self-development through therapy and workshops in search of self-mastery, healing and inner balance.</p>
          <p>My own healing journey began by engaging with the dark parts of my life. I engaged in various certifications - Foundation Course in Expressive Arts therapy, Masters in Psychology, Gestalt therapy, Positive psychology, mindfulness-based coaching, human biography work and Eurythmy. I also reconnected with spirituality and the divine in a renewed way and brought a psycho-spiritual perspective to my life. I now know why my life exists the way it does and I am in charge of my life.</p>
          <p>I founded Poorna Wellbeing in July 2019 with the vision to spread the importance of holistic wellbeing, balance and personal transformation. Today, I engage as an Integral Wellbeing & Life Coach, where I integrate expressive arts, psychology, human biography work, movement & dance, gestalt, esoteric science, and mindfulness in my work with individuals and groups.</p>
          <p>I am passionate about coaching individuals who are seeking to live up to their true full potential. I believe “I am very important to myself and nothing can stop me from being at peace with my true self!” My pursuit to integrate alternative methodologies in the process of self-awareness and transformation makes me a Lifelong Learner.</p>
        </div>
      </div>
        </div>
      </div>
      </div>
    </div>
</section>
<section id="section-our-team" class="section section-intergal intergal-bg section_arrow" style="padding-top: 65px;padding-bottom: 65px;">
  <div class="container">
    <h2 class="px-15 heading-color wow slideInLeft text-left" data-wow-duration="0.5s" data-wow-delay="0.3s">Meet our team</h2>
    <div class="flex-row-center" style="align-items: end;">
      <div class="col-sm-12 col-md-8 text-center">
        <h2 class="heading-color wow slideInUp text-left mt-0 mb-25" data-wow-duration="0.5s" data-wow-delay="0.3s">Noorain Fathima</h2>
        <div class="intergal-box wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">
          <p>Noorain Fathima is counselling psychologist and researcher who aims to understand people and their uniqueness. She has done her Bachelors in Counselling skills from Middlesex University, Dubai and attained her Masters in Counselling Psychology from Montfort college, Bangalore. She is also a certified mental health first aider. </p>
          <p>She focuses on empowerment and highlighting of her client’s strengths and not just problems. In her sessions, she helps focus on the holistic wellbeing of clients.</p>
          <p>Additionally, she has a keen interest in writing research papers and has undertaken several research projects. Her key area of interest is holistic wellbeing and self-compassion.</p>
          <p>Noorain has conducted many online sessions to help raise awareness on mental health and break the stigma around it. She is persistent and will not be stopping anytime soon!</p>
          <p><span class="text-green">Reach out to her at </span><span class="text-blue">noorain@poornawellbeing.com</span></p>
        </div>
      </div>
      <div class="col-sm-12 col-md-4 text-center">
        <div class="team-img"><img src="{{ asset('images/meet-our-team-Noorain-Fathima.png') }}"></div>
        <h3 class="heading-color text-left team-title">Counselling Psychologist <br>
          Researcher <br> Workshop facilitator</h3>
      </div>
    </div>
    <div class="flex-row-center" style="align-items: end;">
      <div class="col-sm-12 col-md-8 text-center">
        <h2 class="heading-color wow slideInLeft text-left mt-20" data-wow-duration="0.5s" data-wow-delay="0.3s">Kamala Narayan</h2>
        <div class="intergal-box wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">
          <p>A cheerful, positive and curious learner is how Kamala defines herself. She is passionate about holistic wellbeing and transformation for self and others. She displays that by masterfully integrating in her life a full-time job in India’s largest conglomerate and volunteering for Poorna Wellbeing.</p>
          <p>Kamala sprinkles life to Poorna Wellbeing by meticulously planning events and workshops and keeping the team on track. With her training as a Positive Psychology coach and integrating her life and work with mindfulness coach training, she helps curate unique interventions that help improve wellbeing of humanity.</p>
          <p>Her life’s purpose is to facilitate positive transformation in people’s lives and live authentically herself! She lives by offering her contribution to Poorna Wellbeing in service of her purpose to herself and humanity at large.</p>
          <p><span class="text-green">Reach out to her at </span><span class="text-blue">kamala@poornawellbeing.com</span></p>
        </div>
      </div>
      <div class="col-sm-12 col-md-4 text-center">
        <div class="team-img"><img src="{{ asset('images/meet-our-team-Kamala-Narayan.png') }}"></div>
        <h3 class="heading-color text-left team-title" style="padding-left: 0px;text-align: center;">Volunteer- Events</h3>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg section-about-bg" id="section-clientele" style="padding-top: 65px;padding-bottom: 65px;">
  <div class="container-fluid">
    <div class="container">
      <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;">Our Clientele</h2>
    </div>
    <div class="row client-main-box">
      <div class="client-logo-container container-fluid">
        <div class="container client-logo-box">
        <div class="client-logo">
          <img src="{{ asset('images/OurClientele1.png') }}">
        </div>
        <div class="client-logo">
          <img src="{{ asset('images/OurClientele2.png') }}">
        </div>
        <div class="client-logo">
          <img src="{{ asset('images/OurClientele3.png') }}">
        </div>
        <div class="client-logo">
          <img src="{{ asset('images/OurClientele4.jpg') }}">
        </div>
        <div class="client-logo">
          <img src="{{ asset('images/OurClientele5.png') }}">
        </div>
      </div>
      </div>
    </div>
  </div>
  </div>
</section>
<section id="section-collaborations" class="section section-intergal intergal-bg section_arrow" style="padding-top: 25px;padding-bottom: 25px;">
  <div class="container">
    <h2 class="px-15 heading-color wow slideInDown text-left mb-25" data-wow-duration="0.5s" data-wow-delay="0.3s">Collaborations</h2>
    <div class="flex-row-center" style="align-items: end;">
      <div class="col-sm-12 col-md-12 text-center">
        <div class="intergal-box wow slideInleft black" data-wow-duration="0.5s" data-wow-delay="0.3s">
          <p>We understand wellbeing in its holistic sense and more often than not, our clients require support in multiple aspects of their life. To enable that, we have collaborated with experts from various spheres who understand and align with the Poorna Wellbeing philosophy. </p>
          <p>Our network extends from career coaching to relationship coaching, psychotherapy, financial management, diet and nutrition, alternative education, parenting and more.</p>
          <p>We believe that together, we achieve more and serve better.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
  num = $('.header_bottom').offset().top + 80;
  $(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
      wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
        wow.init();
  });
</script>
<!-- scroll dwon -->
<script type="text/javascript">
  function headerHeightCalc(){
    var getNiknkHeader = jQuery('#header_bottom').outerHeight();
    var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
    var calcHeaderHei = getNiknkHeader + getwpAdminbar;
    return calcHeaderHei;
  }
  $('.pw-scrollDown button').click(function(){
    var sectionOffset = headerHeightCalc();
    var getHrefVal = jQuery(this).attr('scroll-data-id');
    if(getHrefVal == '#header_bottom'){
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 0},
          'slow');
    }else{
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 78},
          'slow');
    }
    
  });
</script>

<script type="text/javascript">
  
  $('.counter').counterUp({
    delay: 10,
    time: 2000
  });
  $('.counter').addClass('animated fadeInDownBig');
  $('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
  .pb-header-main .navbar-nav>li>a{color: #fff;}
  /*canvas#bg {
      background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
  }*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
