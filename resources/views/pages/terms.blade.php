<!DOCTYPE html>
<html lang="en">
<head>
  <title>Terms & Conditions | Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-event_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="event-banner_head">
					<p>Poorna Wellbeing Event Management</p>
					<h1>Terms & Conditions</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-terms">
	<div class="container">
        <div class="row">
        
            <h3>1. Scope</h3>
            <p>
                With a registered Poorna Wellbeing, India the following terms, and conditions control the offering and selling of products at <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a>. Items purchased on this website are traded directly by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> .
            </p>
            
            <h3>2. General conditions of sale acceptance</h3>
            <p>
                Customers should review general conditions of sale and privacy policies on the given website before making any purchases. If a customer proceeds with a purchase, it is assumed that they have indeed given full acknowledgement and agreement to such policies, and without such acceptance, purchases should not be made. Through purchasing a product or products, customers are accepting the general terms and conditions of the sale, including the payment, as well as reading in full the privacy policy. The customer will have given clear consent for the processing of personal information and agrees to comply with any stipulated agreements, including the payment method. Products offered on <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> website are done so exclusively by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> to persons of or over the age of 18, and who are legally competent to enter into such a binding contract. It is <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> purgative to not process any orders from individuals who fail to meet such criteria.
            </p>
            
            <h3>3. How to purchase</h3>
            <p>
                Any purchase of products completed by customers is done so in coordination with what is displayed and described on the website, including the indicated price and additional taxes and fees, which may vary depending on location. Additional costs include the cost of shipping, all of which will be shown at the moment of purchase. The number of units available will determine the availability of items, and <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> cannot guarantee the delivery of products ordered. <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> will provide customers with the option of reserving products where possible and will communicate fully concerning delivery time (these may be longer than normal) with the customer either by E-Mail or telephone. Before completing any purchase, the customer will always be made aware of the specific unit cost of each product selected, as well as the total number ordered (if more than one) and any delivery charge. Having completed their purchase, customers will receive an E-Mail confirmation of their order, with <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> stating that is has been received, as well as proving a unique order number. After which, customers will receive a second E-Mail, this time confirming the acceptance of any such order, including the delivery of products to the stated address and an estimated delivery date. This information provides clients with the opportunity to check relevant information, and if the necessary report and/or request further information at EMAIL Customers will be able to check the status of the order in the customer area. Orders will have one of the following statuses: CONFIRMED: Order has been made by the Customer and his/her payment has been verified. Order’s will be automatically cancelled by the international banking system should no authorisation be received. WAITING FOR CONFIRMATION <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> has verified the order, thus confirming the availability of the product/s. SENT: Purchases goods have been delivered to the relevant courier for transportation.
            </p>

            <h3>4. Methods of Payment</h3>
            <p>
                Should the customer choose to pay by one of the credit cards options that are accepted and displayed on the website, any payment must be made with the submission of the purchase order. The validity of such a payment method will be stringently checked by the provider during the order process, with an E-Mail being sent to the customer indicating the results. Credit is received by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> at the moment of the shipment of products. Product prices are subject to change, however, confirmed orders will not be affected by any such alterations. Should the provider or institution issuing the credit card refuse payment for any reason, <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> will not be held responsible for any delay or non-delivery of items. <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> reserves the right to automatically cancel any order in the absence of relevant information needed to complete a transaction. Information from the buyer’s credit card or financial information will at no time be known by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a>. Such information is transmitted through a secure and safe connection to an online portal for the bank or finance company provided by the customer. Therefore, <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> will not be held responsible for any fraudulent or illegal use of credit cards which may potentially occur during the transaction of a purchase. Should Paytm be used as a method of payment, customers will be directed immediately to the Paytm login page in order to complete their order. When using Paytm the amount of the purchase is charged to your Paytm account at the time of order. In the case of a cancellation by the customer or non-acceptance by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a>, the amount will be refunded to the Customer’s individual Paytm account. <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> will in no event be liable for any damages, direct or indirect, as a result of any delays in releasing the amount committed by Paytm. For each transaction made with the Paytm account, the customer will receive, by e-mail, confirmation from Paytm directly. <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> reserves the right to cancel and/or suspend the order if Paytm sends an e-mail message showing errors and/or irregularities in the payment. In such an instance, <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> reserves the right to attain additional information from the customer, as well as the sending of documents to prove the ownership of credit cards. <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> also reserves the right not to accept the order without any right for the Customer to claim damages. Should the customer cancel an order (within the stipulated time) reserved with payment by credit card, bank transfer or Paytm, <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> will reimburse through the same chosen method of payment made by the Customer, with requests for other methodology not being permitted under any circumstances. Customer data is processed in a manner which prevents third parties from coming into contact with them. To increase security further, customers should strive to use an internet provider such as Internet Explorer, Google Chrome, Mozilla or Safari. Until the time in which customers confirm any order, data surrounding credit card details reside exclusively on the customer’s computer or device.
            </p>

            <h3>5. Delivery</h3>
            <p>
                <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> can delivery worldwide, however, delivery times may increase depending on the delivery destination. The average delivery time with Maharashtra is 3 Business days and destinations outside Maharashtra such as India the delivery time is 5-7 business days. Weekends (Sat & Sun) & other holidays are non-working days. Invoices for orders delivered by couriers are available to both download and print online from the customer’s individual account area. Once such information has been issued, no changes can be generated. Verification is required if the number of packages delivered corresponds to that shown on the transport document, if the packaging is intact, and/or if the product matches the order. If this is the case, customers must sign the delivery correspondents with” the right to check the integrity of the products”. If this fails to be completed, customers may not dispute the non-matching appearance or quantity of the product ordered after delivery. Orders are processed from <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> 1-2 working days from payment confirmation of the order, always subject to checking the regularity of the payment before carrying it out. In the case of orders with payments in advance other than by bank transfer and where <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> has report of anomalies, such as the mismatch of the name of the person registered and the holder of the credit card, the goods are delivered within 7 working days from when it has been checked for regularity of the payment, or at least from the end of the tests that will be necessary to verify the regularity of payment. In the unlikely event that a product has not been delivered after 10 working days following the order confirmation, customers should send a direct E-Mail with the subject line “non-delivery”, while also stating the order number as well as any other additional information. Having received the E-Mail, <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> will contact the customer within 2 working days in order to resolve the problem or in some cases offer an explanation for non-fulfilment of orders. Custom operations are handled by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> together with the delivery (generally but not compulsorily) customs agency for shipments outside the India. Any documentation needed will be provided by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> to have the package cleared if necessary. With our multiple years of experience of international deliveries and so many international buyers, we can guarantee a fast and smooth delivery. When using Bank Invoice, the amount of the purchase will have to be paid within 48 hours or your order will be cancelled. Once the bank transfer has been confirmed you will receive a notification and your order will be shipped out within 48 hours.
            </p>

            <h3>6. Returns & Refunds</h3>
            <p>
                The returning of products because of differences from what was ordered or product defects will be authorised by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> within one day of delivery. Requests for refunds made after one day of delivery will not be accepted. In order to return such items, customers must contact <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> by E-Mailing to EMAIL Such E-Mail’s should include the date of purchase & order no. The client must also provide photographic evidence for any defected products. The product, along with its original packaging, as well as any other items received must be sent to <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> with proper safety wrapping during courier, in order to protect it from damage. Product received by us in damaged condition will not be acceptable. Any agreed refunds will be made by bank transfer or by Paytm within 15 working days of receipt of goods. Important: The return request should be raised within one day of receiving the order (request can be raised from your account page or via Email) and the item must reach us (Maharashtra for Indian buyers within 5 days & rest of World within 7 days) from return request date. If the customer wants to return the order for a change of mind or because the customer chose the wrong size/product, he/she will be charged for return shipment & restocking fee. If he/she wants to return the order because we have sent the wrong size/product or because the product is damaged, we will pay for return shipment and no restocking fee will be charged. Restocking fee is 10% for Indian buyers..
            </p>
        </div>
	</div>
</section>

<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
	num = $('.header_bottom').offset().top + 80;
	$(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
	    wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
	      wow.init();
	});
</script>
<!-- scroll dwon -->
<script type="text/javascript">
	function headerHeightCalc(){
	  var getNiknkHeader = jQuery('#header_bottom').outerHeight();
	  var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
	  var calcHeaderHei = getNiknkHeader + getwpAdminbar;
	  return calcHeaderHei;
	}
	$('.pw-scrollDown button').click(function(){
	  var sectionOffset = headerHeightCalc();
	  var getHrefVal = jQuery(this).attr('scroll-data-id');
	  if(getHrefVal == '#header_bottom'){
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 0},
	        'slow');
	  }else{
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 78},
	        'slow');
	  }
	  
	});
</script>

<script type="text/javascript">
	
	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});
	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
	.pb-header-main .navbar-nav>li>a{color: #fff;}
	/*canvas#bg {
	    background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
	}*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
