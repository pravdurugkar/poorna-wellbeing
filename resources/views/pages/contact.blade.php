<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contact Us | Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
  @include('nav.menu')
</div>
<section id="section-our-logo" class="section section-intergal intergal-bg section_arrow" style="padding-top: 0px;padding-bottom: 0px;">
  <div class="poornawellbeing-model-img" style="background-image: url('{{ asset('images/contact-bg.jpg') }}');background-size: cover;height: 550px;position: relative;">
    <div class="overlay"></div>
    <div style="position: absolute;bottom: 160px;left: 42px;">
      <h1 class="text-black wow slideInDown text-center mb-0" data-wow-duration="0.5s" data-wow-delay="0.3s" style="font-size: 50px;text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;"><strong>Contact Us</strong></h1>
      <h4 style="font-size: 30px;text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;"><strong>We look forward to hear from you</strong></h4>
    </div>
  </div>
</section>
<section class="section section-about" id="section-our-logo" style="padding-top: 65px;padding-bottom: 65px;">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <h2 class="heading-color text-left mb-25"> Send Us A Message</h2>
          <form>
            <div class="form-group">
              <label for="exampleInputEmail1" class="h4"><strong>Name <span style="color: red;">*</span></strong></label>
              <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control custom-input">
                  <small class="form-text text-muted">First</small>
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control custom-input">
                  <small class="form-text text-muted">Last Name</small>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1" class="h4"><strong>Contact No<span style="color: red;">*</span></strong></label>
              <div class="row">
              <div class="col-md-6">
                <input type="email" class="form-control custom-input">
              </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1" class="h4"><strong>Email Id<span style="color: red;">*</span></strong></label>
              <div class="row">
              <div class="col-md-6">
                <input type="email" class="form-control custom-input">
              </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1" class="h4"><strong>Comment or Message<span style="color: red;">*</span></strong></label>
              <div class="row">
              <div class="col-md-12">
                <textarea class="form-control custom-input custom-text-area"></textarea>
              </div>
              </div>
            </div>
            <button class="btn-submit">Submit</button>
          </form>
        </div>
      </div>
      <div class="col-md-5 offset-md-1">
        <div class="" style="display: flex;margin-top: 20px;">
          <i class="fa fa-map-marker contact-icon"></i>
          <h3 class="heading-color mt-0 mb-0"><strong>Visit Us</strong></h3>
        </div>
        <h4 class="address-text mb-0">B4-1313, Gokulam Apartments Vasanta Vallabha Nagar Kuvempu Nagar Road Kanakapura Road Bangalore 560062</h4>
        <div class="" style="display: flex;margin-top: 20px;">
          <i class="fa fa-envelope-o contact-icon"></i>
          <h3 class="heading-color mt-0 mb-0"><strong>Email Us</strong></h3>
        </div>
        <h4 class="address-text mt-0">poornawellbeing@gmail.com</h4>
        <div class="" style="display: flex;margin-top: 20px;">
          <i class="fa fa-mobile contact-icon"></i>
          <h3 class="heading-color mt-0 mb-0"><strong>Give Us a Call</strong></h3>
        </div>
        <h4 class="address-text mt-0 mb-0">+91-9686051005</h4>
        <div class="card map-box">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.312734026645!2d77.54955214992212!3d12.88760112015933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3fe57203c445%3A0x829793a661fd92a7!2sGokulam%20Apartments!5e0!3m2!1sen!2sin!4v1608439869062!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-about" id="section-our-logo" style="padding-top: 10px;padding-bottom: 40px;">
  <h1 class="heading-color text-center mb-25"><strong>Thank you Wellbeing Seekers! </strong></h1>
</section>
<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
  num = $('.header_bottom').offset().top + 80;
  $(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
      wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
        wow.init();
  });
</script>
<!-- scroll dwon -->
<script type="text/javascript">
  function headerHeightCalc(){
    var getNiknkHeader = jQuery('#header_bottom').outerHeight();
    var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
    var calcHeaderHei = getNiknkHeader + getwpAdminbar;
    return calcHeaderHei;
  }
  $('.pw-scrollDown button').click(function(){
    var sectionOffset = headerHeightCalc();
    var getHrefVal = jQuery(this).attr('scroll-data-id');
    if(getHrefVal == '#header_bottom'){
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 0},
          'slow');
    }else{
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 78},
          'slow');
    }
    
  });
</script>

<script type="text/javascript">
  
  $('.counter').counterUp({
    delay: 10,
    time: 2000
  });
  $('.counter').addClass('animated fadeInDownBig');
  $('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
  .pb-header-main .navbar-nav>li>a{color: #fff;}
  /*canvas#bg {
      background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
  }*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
