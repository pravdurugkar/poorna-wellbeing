<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-event_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="event-banner_head">
					<p>{{ $event->title }}</p>
					<h1>Event Booking</h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-event_listing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="event-list_head">
					<!-- <h2>Upcoming Events</h2>
					<p>These are just some of the numerous reasons of choose us.</p> -->
				</div>
			</div>
		</div>
		<div class="row grid-wrapper">
			
			<div class="event-booking-form grid-item">
				<form id="event-booking" method="post">
					@csrf
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						    	<label for="first-name">First Name</label>
						    	<input type="text" class="form-control" id="first-name" placeholder="John">
						  	</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						    	<label for="last-name">Last Name</label>
						    	<input type="text" class="form-control" id="last-name" placeholder="Doe">
						  	</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="exampleFormControlInput1">Email address</label>
						    	<input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
						  	</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="total-seats">Total Seats</label>
								<select class="form-control" id="total-seats">
								  <option>1</option>
								  <option>2</option>
								  <option>3</option>
								  <option>4</option>
								  <option>5</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
						  	<div class="form-group">
						    	<label for="mobile-number">Mobile</label>
						    	<input type="tel" class="form-control" name="mobile-numbers" id="mobile-number" placeholder="9876543210">
						  	</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="how-you-know">How did you come to know of this event?</label>
								<select class="form-control" id="how-you-know">
								  <option>Through facilitator</option>
								  <option>Social Media</option>
								  <option>Friends or Relatives</option>
								  <option>Advertisements</option>
								  <option>Other</option>
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
						  	<div class="form-group">
						    	<input type="checkbox" name="chk-subscribe" id="chk-subscribe">
						    	<label for="chk-subscribe">Subcribe for Newsleter</label>
						  	</div>
						</div>
					</div>

				  	<!-- <div class="form-group">
				    	<label for="exampleFormControlTextarea1"></label>
				    	<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
				  	</div> -->

					<div class="form-group">
					  	<button type="submit" class="btn btn-primary">Book Event</button>
					</div>
				</form>
			</div>

    	</div>
	</div>
</section>

<!-- Footer -->
@include('layouts.footer')
<!-- END -->


<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>