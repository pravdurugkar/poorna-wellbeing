<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing Model | Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
  @include('nav.menu')
</div>
<!-- <section class="section-event_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="event-banner_head">
          <p>Poorna Wellbeing Event Management</p>
          <h1>Poornawellbeing-model</h1>
        </div>
      </div>
    </div>
  </div>
</section> -->
<section id="section-our-logo" class="section section-intergal intergal-bg section_arrow" style="padding-top: 65px;padding-bottom: 65px;">
  <div class="container">
    <h2 class="px-15 heading-color wow slideInDown text-center mb-0 " data-wow-duration="0.5s" data-wow-delay="0.3s" style="font-size:33px;">Poorna Wellbeing Model</h2>
    <h4 class="text-center mb-25">Embrace Self Mastery with Wellbeing & Balance</h4>
    <div>
      <div class="col-sm-12 col-md-12 text-center">
          <div class="poornawellbeing-model-img"><img src="{{ asset('images/poornawellbeing-model.png') }}"></div>
      </div>
      <div class="col-sm-12 col-md-12 text-center">
        <h2 class="px-15 heading-color wow slideInDown text-center mb-25" data-wow-duration="0.5s" data-wow-delay="0.3s">Introduction</h2>
          <div class="intergal-box wow slideInUp black" data-wow-duration="0.5s" data-wow-delay="0.3s">
            <p>Human being’s innate seeking for balance, wholeness and completeness is the seed for the journey towards Self Mastery and Well Being. The Poorna Wellbeing Model offers guidance on aspects that enhance and restore holistic wellbeing and strengthen Self Mastery.</p>
            <p>The journey of Self-mastery is the journey of remembering, invoking, and staying tuned to the Magnificent and Radiant Core Self. In case the tuning is lost, connection is restored using the tools of Grounding, Self-reflection, Joyful rhythms, and Conscious Learning.</p>
            <p>The 3 inner layers represent aspects where imbalances could occur in our life; the journey of Self-mastery is to become aware and take actions to restore balance while staying connected to the Core Self.</p>
          </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg" id="section-our-story">
  <div class="container-fluid">
    <div class="row flex-row-center">
      <div class="approach-box-sec">
        <div class="content bg-white" style="padding: 0px;">
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;">The Center of Our Self - Our Radiant Core</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“The core of true self is never lost. Let go off all the pretending and becoming you’ve done just to belong. Curl up with rawness and come home. You don’t have to find yourself. You just have to let yourself in."</i></h4>
            <div>
            <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                <p class="mb-25">Our Core Self is who we truly are when we were born. This radiant, magnificent self is untouched by our persona, ambitions, relationships, biases, or any conditioning from the outer world. This space of original inner light carries the resonance of joy, vibrance, strength, light, vastness, love, harmony, rhythm. Connecting with and expressing our Core brings us deep fulfilment, healing, and transformation.</p>
                <p><strong class="text-black">What are your key qualities that make up your essence?</strong></p>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg" id="section-our-story">
  <div class="container-fluid">
    <div class="row flex-row-center">
      <div class="approach-box-sec">
        <div class="content" style="padding: 0px;">
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;top:-90px;">The Periphery</h2>
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Grounding</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“It is only by grounding our awareness in the living sensation of our bodies that the ‘I Am,’ our real presence, can awaken."</i></h4>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Grounding is the quality of being rooted in our body, mind, and senses, thus practicing equanimity. Just like an oak tree standing tall and unshaken in a storm, grounding helps to stay in the present moment and not be affected by the vagaries of the outer world. Grounding helps us to overcome overwhelming feelings and incessant thinking. Simple techniques can help us restore awareness and connect to our roots.</p>
                  <p><strong class="text-black">Have you ever tried taking a 5 minutes break on a workday and gazed at the blue sky?</strong></p>
              </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Self-Reflection</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“Self-reflection is the gateway to freedom"</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Self-reflection is the practice of self-examination, thinking objectively about our beliefs and behaviour, that paves the path towards personal growth and self-mastery. The practice of self-reflection includes observation of thoughts and emotions with openness and objectivity. It helps develop insight and a deep understanding of “Why we did what we did”, thereby providing a possibility to overcome limiting behaviours and replace biased thinking processes.</p>
                  <p><strong class="text-black">What new life lessons/skills have you learnt in the last 6 months?</strong></p>
              </div>
            </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Joyous Rhythms</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“To live is to be musical, starting with the blood dancing in your veins. Everything living has a rhythm. Do you feel your music?"</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>The world around us - Mother Nature and within us - our daily routine, moves along a rhythm. These rhythms keep us secure and sane. When rhythms of breath, activity, food and nutrition, relationships and self-reflection are balanced, we are on the joyous path of Self-mastery.</p>
                  <p><strong class="text-black">Have you skipped your routine and felt it in your mood?</strong></p>
              </div>
            </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Learning Journeys</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“We are not perfect, we are learning. That’s the beauty in our specific journey."</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>The beauty of our journey lies in the continuous process of learning from our life experiences, our relationships, and patterns. Asking appropriate questions, objectively reflecting on it, and looking at it from different perspectives can help uncover the hidden pearls that are needed for evolving. We like to call it “Learning from the past for achieving the desired future”.</p>
                  <p><strong class="text-black">Take a difficult relationship you are currently dealing with; can you look at the memory you have with them from another perspective and see what emerges?</strong></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-about section-about-bg" id="section-our-logo">
  <div class="container">
    <div>
      <div class="col-sm-12 col-md-12 about-text-box">
        <h2 class="mb-25 px-15">Self from Diffrent Perspective</h2>
        <div>
          <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text perspective-text">
            <p class="mb-0">We are a complex system of multiple thoughts, emotions, functions, and aspirations.</p>
            <p>In the journey of Self-mastery, bringing our awareness to multiple layers of Self is necessary</p>
            <ul class="text-left perspective">
              <li>Two Layered Self – Being & Doing</li>
              <li>Three Layered Self – Thinking, Feeling & Willing</li>
              <li>Five layered Self – Physical, Etheric, Emotional, Intellectual, Spiritual</li>
            </ul>
          </div>
          </div>
      </div>
      <div class="col-sm-12 col-md-12 text-center">
        <div class="perspective-diagram"><img src="{{ asset('images/self-from-different-perspective-diagram-final.png') }}"></div>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg" id="section-our-story">
  <div class="container-fluid">
    <div class="row flex-row-center">
      <div class="approach-box-sec">
        <div class="content" style="padding: 0px;">
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;top:-80px;">Two Layered Self</h2>
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Being</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“In the being mode, the mind has “nothing to do, nowhere to go” and can focus fully on moment-by-moment experience, allowing us to be fully present and aware of whatever is here, right now."</i></h4>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>It is our inner “Sacred Space” that holds the awareness of the Core self, values, beliefs, desires for fulfilment & joy, and Life purpose. From this space, we access our connection with the universal energy and become aware of the higher self, which is beyond biases, conditioning, and limitations.</p>
                  <p><strong class="text-black">What is your most resourceful place and/or activity when you feel very connected with yourself?</strong></p>
              </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Doing</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“Once you shift your order of priorities from “having-doing-being” to “being-doing-having,” your destiny will be in your hands."</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Doing is akin to the building that gets constructed on the foundation of Being. It represents a goal oriented and action-based life, primarily focused on external achievement. Goal Setting & achievement (Doing) in awareness and alignment to the Core self (Being) presents a holistic picture on the journey of self-mastery, fulfilment, and personal transformation.</p>
                  <p><strong class="text-black">What are your short-term goals (next one year) and your long-term goals (next five years)?</strong></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg section-about-bg" id="section-our-story">
  <div class="container-fluid">
    <div class="row flex-row-center">
      <div class="approach-box-sec about-us-box">
        <div class="content" style="padding: 0px;">
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;top:-80px;">Three Layered Self</h2>
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Thinking</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“You have powers you never dreamed of. You can do things you never thought you could do. There are no limitations in what you can do except the limitations of your own mind."</i></h4>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Thinking is the use of analytical thinking, conceptual thinking and observation for decision making. It is a conscious activity that holds potential to either make decisions in alignment with our highest purpose. The journey of Self Mastery is to apply just the right measure of thinking in daily life and balance it with other faculties available with the Self.</p>
                  <p><strong class="text-black">Have you ever overthought a situation before it even happened?</strong></p>
              </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Willing/ Acting</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“Willpower is like a muscle: The more you train it. The stronger it gets."</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>The intention to act (combination of courage, mental stamina, determination) and taking actions brings in Will in our life. Exercising the capacity of will boosts self-confidence and enhances positive life outcomes like better performance and improved physical and mental health. Acting without thinking is mindless action.</p>
                  <p><strong class="text-black">Do you have many goals but struggle to stay focussed and take action? What can you commit to make your goals come true?</strong></p>
              </div>
            </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Feeling</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“Always be true to your feelings because the more you deny what you feel the stronger it becomes"</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Feeling is the seat of empathy that acts as a bridge between thinking and willing. Staying tuned with our feelings, enables us to let loose in a rhythmic manner. An emotionally regulated human being is someone who has cultivated the art of managing their feelings and lives life in a balanced manner.</p>
                  <p><strong class="text-black">Have a quick check in with how you feel?</strong></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-aprroach section-aprroach-bg" id="section-our-story">
  <div class="container-fluid">
    <div class="row flex-row-center">
      <div class="approach-box-sec">
        <div class="content" style="padding: 0px;">
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-25" style="position: relative;z-index:1;top:-80px;">Five layered Self</h2>
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Physical</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“To keep the body in good health is a duty…otherwise we shall not be able to keep the mind strong and clear."</i></h4>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Physical wellbeing is a tangible aspect that can boost our mood and ignite motivation. Focusing on daily rhythms of sleep, nutrition, exercise, and rest keeps the physical body in great health. The journey towards self-mastery definitely needs a robust physical body with balanced activity and rest.</p>
                  <p><strong class="text-black">What is your daily physical wellbeing routine?</strong></p>
              </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Etheric</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“The etheric body is the fire body. It is the natural envelope of the soul."</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Etheric or energy self is characterized by vitality of the body and vibrance in the being. The etheric self, also known as the ‘energetic blueprint’ of the physical self, is responsible for the “vibe” we emit when we meet people. Healthy etheric helps to deal with diseases, increases immunity, and keeps toxins (anxious thoughts and feelings) at bay.</p>
                  <p><strong class="text-black">How often do you consciously breathe? Or is breathing an involuntary activity for you?</strong></p>
              </div>
            </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Emotional</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“It isn't stress that makes us fall--it's how we respond to stressful events."</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Emotional self focuses on three critical relationships: relationship with self, with primary relationships and with other relationships. Emotions hold the potential to living in awareness but if unregulated, living is triggered and fearful. Healthy emotional life enhances stability, thought clarity and purposeful living. Living with deep emotional awareness enhances emotional mastery, a key element of Self -mastery.</p>
                  <p><strong class="text-black">How are you feeling right now?</strong></p>
              </div>
            </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Intellectual</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“The creative adult is the child who survived"</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                  <p>Intellectual self-thrives on having meaningful opportunities to learn and grow in personal and professional life. Self-awareness plays a critical role in inquiring deep into the core values, beliefs, career anchors. Acting in accordance with Core self-enhances the fulfilment of this aspect of self and enhance congruence. Living in wonder and curiosity is a key ingredient here.</p>
                  <p><strong class="text-black">What new opportunities to learn and grow have opened up for you in the last 6 months?</strong></p>
              </div>
            </div>
          </div>
          <div class="container">
            <h2 class="px-15 heading-color text-left mb-0" style="position: relative;z-index:1;">Spiritual</h2>
            <h4 class="text-left px-15" style="position: relative;z-index:1;"><i class="text-brown">“Create a life that feels good on the inside not just one that looks good on the outside"</i></h4>
            <div>
              <div class="col-xs-12 col-sm-12 col-md-12 approach-box-right approach-box-text poornwellbeing-model-text">
                <p>Spiritual self is about living with an abundance mindset bringing in a sense of prosperity and wellbeing. Living with a deep sense of purpose, being in tune with the connection within self and with the divine power is another aspect of the spiritual self. A deep knowing of “Who I am in the depths of my being” enhances the quality of life and positive feelings like hope and satisfaction.</p>
                <p><strong class="text-black">What are your practices to actively connect with the Divine every day?</strong></p>
                <p class="text-center"><strong><i class="text-green">Self-Mastery is a lifelong journey and balance of all these aspects <br>of self is the recipe of living fulfilled, joyous and congruent!</i></strong></p>
                <p class="text-center"><strong><i class="text-brown">Consciousness is the key!</i></strong></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
  num = $('.header_bottom').offset().top + 80;
  $(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
      wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
        wow.init();
  });
</script>
<!-- scroll dwon -->
<script type="text/javascript">
  function headerHeightCalc(){
    var getNiknkHeader = jQuery('#header_bottom').outerHeight();
    var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
    var calcHeaderHei = getNiknkHeader + getwpAdminbar;
    return calcHeaderHei;
  }
  $('.pw-scrollDown button').click(function(){
    var sectionOffset = headerHeightCalc();
    var getHrefVal = jQuery(this).attr('scroll-data-id');
    if(getHrefVal == '#header_bottom'){
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 0},
          'slow');
    }else{
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 78},
          'slow');
    }
    
  });
</script>

<script type="text/javascript">
  
  $('.counter').counterUp({
    delay: 10,
    time: 2000
  });
  $('.counter').addClass('animated fadeInDownBig');
  $('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
  .pb-header-main .navbar-nav>li>a{color: #fff;}
  /*canvas#bg {
      background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
  }*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
