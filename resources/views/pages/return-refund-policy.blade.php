<!DOCTYPE html>
<html lang="en">
<head>
  <title>Return & Refund Policy | Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-event_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="event-banner_head">
					<p>Poorna Wellbeing Event Management</p>
					<h1>Return & Refund Policy</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-terms">
  <div class="container">
    <div class="row">
      <h3>1. Refund Policy</h3>

      <p>Important: The return request should be raised within Two days of receiving the order (request can be raised by sending email to <a href="mailto:support@poornawellbeing.com">support@poornawellbeing.com</a>) and once the return approval is granted, the item must reach us (Vimal Digital Photo Lab (Maharashtra State) for Indian buyers within 5 days &amp; rest of World within 7 days) from return request approval date. If the buyer wants to return the order for a change of mind or because the buyer chose the wrong size/product, the buyer will be charged for return Shipment &amp; Restocking fee. If the buyer wants to return the order because we have sent the wrong size/product or because the product is damaged, we will pay for return Shipment and no Restocking fee will be charged. Restocking fee is 10% of Purchase Value for Indian buyers.</p>

      <p>Return request E-mails should include the date of purchase &amp; order no. The buyer must also provide photographic evidence for any defected products. The product, along with its original packaging, as well as any other items received must be sent to <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> with proper safety wrapping during courier, in order to protect it from damage. Product received by us in damaged condition will not be accepted. Any agreed refunds will be made by bank transfer or by Paytm within 15 working days of receipt of goods.</p>

      <h3>2. CANCELLATION POLICY</h3>
      <p>To reduce last-minute cancellations and the risk of chargebacks for customers, it is always a good idea to carefully read all policies. The cancellation of products because of differences from what was ordered or product defects will be authorized by <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> wwithin one day of delivery. Requests for refunds made after one day of delivery will not be accepted. In order to return such items, customers must contact <a href="https://www.poornawellbeing.com/">www.poornawellbeing.com</a> by E-Mailing to EMAIL.</p>

    </div>
  </div>
</section>

<!-- Footer -->
@include('layouts.footer')
<!-- END -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
	num = $('.header_bottom').offset().top + 80;
	$(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
	    wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
	      wow.init();
	});
</script>
<!-- scroll dwon -->
<script type="text/javascript">
	function headerHeightCalc(){
	  var getNiknkHeader = jQuery('#header_bottom').outerHeight();
	  var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
	  var calcHeaderHei = getNiknkHeader + getwpAdminbar;
	  return calcHeaderHei;
	}
	$('.pw-scrollDown button').click(function(){
	  var sectionOffset = headerHeightCalc();
	  var getHrefVal = jQuery(this).attr('scroll-data-id');
	  if(getHrefVal == '#header_bottom'){
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 0},
	        'slow');
	  }else{
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 78},
	        'slow');
	  }
	  
	});
</script>

<script type="text/javascript">
	
	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});
	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
	.pb-header-main .navbar-nav>li>a{color: #fff;}
	/*canvas#bg {
	    background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
	}*/
</style>
<script type="text/javascript">
      jQuery('').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
</script>
</body>
</html>
