<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow" id="header_bottom">
	@include('nav.menu')
</div>
<section class="section-page_bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="offering-banner_head">
					<h1>Offerings</h1>
					<p>The four premium offerings for individuals are the USP of Poorna Wellbeing. They focus on self-awareness, sustained practice and transformation. </p>
					<p>They bring holistic Wellbeing to the individual.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section section_offering_v4">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="offering-tab-wrap">
					<ul class="nav nav-stacked col-md-3">
	                    <li class="active"><a href="javascript:void(0)" data-href="#a" data-toggle="tab"> <img src="{{ asset('images/offering/user.png') }}"><span>Individual</span> <i class="fa fa-minus" aria-hidden="true"></i></a>
	                    	<ul class="pb-nav-dropdown open">	                    			
	                    		<li class="active"><a href="javascript:void(0)" data-href="#navdd1" data-toggle="tab"> Integral Wellbeing Coaching </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd2" data-toggle="tab"> Career Transitions Coaching  </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd3" data-toggle="tab"> Envisioning Desired Future  </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd4" data-toggle="tab"> Rhythms for Wellbeing </a></li>
	                    	</ul>
	                    </li>
	                    <li><a href="javascript:void(0)" data-href="#b" data-toggle="tab"> <img src="{{ asset('images/offering/group.png') }}"><span>Group</span><i class="fa fa-plus" aria-hidden="true"></i></a>
	                    	<ul class="pb-nav-dropdown">	                    			
	                    		<li><a href="javascript:void(0)" data-href="#navdd5" data-toggle="tab"> Collaboration and Team Visioning </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd6" data-toggle="tab"> Dancing Rhythms of togetherness  </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd7" data-toggle="tab"> Career Lab   </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd8" data-toggle="tab"> Women’s Self Leadership </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd9" data-toggle="tab"> Customized Wellbeing Interventions </a></li>
	                    	</ul>
	                    </li>
	                    <li><a href="javascript:void(0)"  data-href="#c" data-toggle="tab"><img src="{{ asset('images/offering/comm.png') }}"><span>Community</span><i class="fa fa-plus" aria-hidden="true"></i></a>
	                    	<ul class="pb-nav-dropdown">	                    			
	                    		<li><a href="javascript:void(0)" data-href="#navdd10" data-toggle="tab"> Poorna Wellbeing circle </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd11" data-toggle="tab"> Poorna wellbeing Utsava  </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd12" data-toggle="tab"> Poorna wellbeing Samvaada    </a></li>
	                    		<li><a href="javascript:void(0)" data-href="#navdd13" data-toggle="tab"> Poorna Wellbeing Utsava for Teams </a></li>
	                    	</ul>
	                    </li>
	                </ul>
	                <div class="tab-content col-md-9">
	                    <div class="tab-pane active" id="navdd1">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Integral Wellbeing Coaching</h2>
	                    			<p class="small-big"><strong>Embark on a Self Mastery journey</strong></p>
	                    			<p><strong>Needs of the client  </strong></p>
	                    			<ul>
	                    				<li>Seeking to live a life of balance and personal growth</li>
	                    				<li>Been overwhelmed in life circumstances and lost their center and seeking their way back</li>
	                    				<li>Seeking to life a life of balance and personal growth </li>
 										<li>Have hit a roadblock in life – at work, in relationships, in career, in personal growth and seek help to progress further </li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    		<div class="offering-tab-fulle col-md-12">
	                    		<p><strong>The coaching process</strong></p>
	                    			<ul>
	                    				<li>Help client view their life as a whole – the present, the past and future by integrating various cognitive and experiential methodologies like instruments, expressive arts – movement, theatre, visual arts, to name a few </li>
	                    				<li>Help client see themselves as a whole human being, focusing on the physical body, the emotional body, the intellectual body and the spiritual essence </li>
	                    				<li>Bring holistic practices and psycho education in the session and encourage their practice in daily life to enhance self-awareness and personal mastery </li>
	                    			</ul>
	                    			<p><strong>The results or benefits  </strong></p>
	                    			<ul>
	                    				<li>Clarity on their personal vision, values and strengths</li> 
										<li>Develop dynamic self-awareness as a method to connect to their body, emotions and relationships </li> 
										<li>Experience their inner radiance and power and make it visible in the world</li>
	                    			</ul>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd2">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Career Transitions Coaching</h2>
	                    			<p class="small-big"><strong>Embark on a transformative Career clarification journey</strong></p>
	                    			<p><strong>Needs of the client</strong></p>
	                    			<ul>
	                    				<li>Seeking a career shift or role shift and wants to reinforce one’s natural career inclinations and talents</li>
	                    				<li>Experiencing lack of joy at work and seeks to find the source of dissonance and path forward</li>
	                    				<li>Experiencing a crisis at work and wants a pause before making career choices </li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}">
	                    			</div>
	                    		</div>
	                    		<div class="offering-tab-fulle col-md-12">
	                    		<p><strong>The coaching process</strong></p>
	                    			<ul>
	                    				<li>Enable the client reflect on one’s Career Journey and discover ones Career values using the Career Anchor inventory and interview process</li>
	                    				<li>Identity one’s Personal Values using expressive arts methodologies </li>
	                    			</ul>
	                    			<p><strong>The results or benefits  </strong></p>
	                    			<ul>
	                    				<li>Clarity and conviction on the way forward in one’s career journey  </li> 
	                    			</ul>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd3">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Envisioning Desired Future</h2>
	                    			<p class="small-big"><strong>Embark on a Future Visioning journey</strong></p>
	                    			<p><strong>Needs of the client</strong></p>
	                    			<ul>
	                    				<li>At different life situations, client seek clarity on their life to enable them make clear and powerful life choices</li>
	                    				<li>Life situations could be
	                    					<ul>
	                    						<li>Entered into new relationship – newly wed / becoming a parent / life after a separation. Want to create a desirable image of the future </li>
	                    						<li>Entered a new year and want to create a powerful desired future image to take charge of life</li>
	                    						<li>Entered into a new job</li>
	                    						<li>High school and college Students who want to live their school and college life with awareness and focus </li>
	                    					</ul>
	                    				</li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    		<div class="offering-tab-fulle col-md-12">
	                    		<p><strong>The coaching process</strong></p>
	                    			<ul>
	                    				<li>Enable the client review and clearly see the past</li>
	                    				<li>Journey the client to dream their desired future through non cognitive methodologies like visualization and imagination, expressive arts – visual art, movement and theatre </li>
	                    				<li>Prototype the envisioned image through expressive arts methodologies </li>
	                    			</ul>
	                    			<p><strong>The results or benefits  </strong></p>
	                    			<ul>
	                    				<li>Clear and compelling image of the desired future</li> 
	                    				<li>Clear first steps and the next few steps articulated by the client</li>
	                    			</ul>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd4">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Rhythms for Wellbeing and Self-care </h2>
	                    			<p class="small-big"><strong>Develop Resilience and Presence through Holistic Wellbeing Rhythms</strong></p>
	                    			<p><strong>Needs of the client</strong></p>
	                    			<ul>
	                    				<li>Begin the process of Wellbeing and Self Care but needs help and support to make that beginning</li>
	                    				<li>To emerge out of a major life crisis through sustained Wellbeing practices </li>
	                    				<li>To bring and adhere to a Wellbeing discipline now and forever in their life</li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    		<div class="offering-tab-fulle col-md-12">
	                    		<p><strong>The rhythmic process</strong></p>
	                    			<ul>
	                    				<li>Conversation with the client to understand the current state and desired state and contract</li>
	                    				<li>Co-create with the client commit to the weekly rhythms. At a time only one week’s rhythms are decided upon</li>
	                    				<li>Communication with client every evening through whatsapp message and at the end of every week on a video call to review the impact and progress on the client</li>
	                    				<li>Conclusion of the program (4 weeks ) and way forward</li>
	                    			</ul>
	                    			<p><strong>The results or benefits  </strong></p>
	                    			<ul>
	                    				<li>In charge of ones physical, emotional and spiritual self </li> 
	                    				<li>Joy of practicing wellbeing consciousness and self care </li>
	                    				<li>Heightened self-awareness and resilience </li>
	                    			</ul>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd5">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Group Dynamic Facilitation</h2>
	                    			<p>Designed to suit the needs of the client requirements, they are offered in various formats and lengths depending on where this features in the overall intervention </p>
	                    			<p>They can be individually implemented or woven together as a part of a holistic intervention </p>
	                    			<h2>Collaboration and Team Visioning</h2>
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box"><img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    		<div class="offering-tab-fulle col-md-12">
	                    		<p class="small-big"><strong>Goal </strong></p>
	                    			<p>Co-create, Clarify and/or align the employees of an organization and/or towards a shared vision and foster collaboration</p>
	                    			<p><strong>The Process</strong></p>
	                    			<ul>
	                    				<li>Stakeholder discussion, diagnostic interviews, employee surveys study to assess current state from collaboration/cohesion perspective or the lack of it </li>
	                    				<li>Co-creating the desired state based on the business, employees and the customer context </li>
	                    				<li>Customized workshop with the team, providing unique experiences using integrative methodologies to enable collaboration </li>
	                    				<li>Action projects, intermittent workshops and/or dialogue spaces to shift from current state to desired state.</li>
	                    			</ul>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd6">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Dancing Rhythms of togetherness</h2>
	                    			<p><strong>Goal </strong></p>
	                    			<p class="small-big"><strong>Bring the community together through rhythmical movements to folk beats</strong></p>
	                    			<p><strong>The process </strong></p>
	                    			<ul>
	                    				<li>The group gathers together led by a facilitator</li>
	                    				<li>Movement sequence from warm ups which prepares the body to groove to rhythmical movements to folk beats</li>
	                    				<li>Experience the power of positive group energy and a felt sense of letting go, lightness and emotional wellbeing </li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd7">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Career Lab</h2>
	                    			<p><strong>Goal </strong></p>
	                    			<p>To enable working professionals (in-company team or heterogenous group of professionals) gain clarity on their Career orientations, personal values, beliefs and craft a compelling desired future</p>
	                    			<p><strong>The process </strong></p>
	                    			<ul>
	                    				<li>Pre lab briefing to the participants and their managers ( if it is an in-company program) </li>
	                    				<li>2 or 3 days workshop (combining western and eastern methodologies like art, movement, trekking, walking, psychological instruments, silence, meditation,  in a serene location close to nature</li>
	                    				<li>Post workshop support through review calls, buddy sharing</li>
	                    				<li>1-day closure workshop after 6 months </li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box"><img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd8">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Women’s Self Leadership </h2>
	                    			<p><strong>Goal </strong></p>
	                    			<ul>
	                    				<li>Enhance awareness of Self – Beliefs, values, strengths</li>
	                    				<li>Practice the art of deep listening to self and others</li>
	                    				<li>Articulate Personal Vision</li>
	                    				<li>Practice the art of Presencing </li>
	                    				<li>Act in congruence with Vision, Beliefs and Values and take charge of life</li>
	                    			</ul>
	                    			<p><strong>The process </strong></p>
	                    			<ul>
	                    				<li>Diagnosis – context and need of the intervention, stakeholder interviews</li>
	                    				<li>Design the journey comprising of workshops, WhatsApp group conversations, buddy interactions, coaching sessions </li>
	                    				<li>Measure intervention effectiveness (co-created alongwith the leadership development team)</li>
	                    			</ul>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box"><img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd9">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Customized Wellbeing Interventions</h2>
	                    			<p><strong>Goal </strong></p>
	                    			<ul>
	                    				<li>Enhance the sense of inner alignment and harmony thus bringing about higher efficiency and effectiveness at work</li>
	                    				<li>De-stress oneself, the team </li>
	                    				<li>Address any aspect that erodes wellbeing of self, team and organization</li>
	                    			</ul>
	                    			<p><strong>The process </strong></p>
	                    			<p>Identify the specific wellbeing aspects to be address (physical, intellectual, emotional or spiritual) </p>
	                    			<p>Design and delivery of the intervention</p>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd10">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Poorna Wellbeing circle</h2>
	                    			<p>Poorna Wellbeing Circle is a safe space offered monthly to explore one’s mind-body connection through expressive arts, spiritual practices and more. This monthly event is based on a Holistic Wellbeing model and enables you to create a rhythm of self-exploration.</p>
	                    			
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}">
	                    			</div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd11">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Poorna wellbeing Utsava</h2>
	                    			<p>Poorna Wellbeing Utsava is a celebration of holistic wellbeing offered to the community of wellbeing seekers on “Pay as you wish” basis. It enables exposure to methodologies and facilitators from various Wellbeing streams under one roof. A mix of theoretical, experiential and reflective sessions makes it participative and enriching for the participants.</p>
	                    			<p>It is held annually in Bangalore in the last quarter of the calendar year. </p>
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}">
	                    			</div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd12">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Poorna wellbeing Samvaada</h2>
	                    			<p>We organize Wellbeing conversations, dialogues for perspective building on overall Wellbeing or specific themes and modalities. This would be through the Poorna Wellbeing YouTube, Instagram and facebook channel and available for viewing at people’s convenience. </p>
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box"><img src="{{ asset('images/offering/individual.jpg') }}"></div>
	                    		</div>
	                    	</div>
	                    </div>
	                    <div class="tab-pane" id="navdd12">
	                    	<div class="offering-tab-box">
	                    		<div class="offering-tab-left col-md-6">
	                    			<h2>Poorna Wellbeing Utsava for Teams </h2>
	                    			<p>We offer consultancy and facilitation services to conduct Wellbeing festivals (Utsava) in your organization or team premises or online. This enables employees and teams to rejuvenate, refresh themselves and throw away accumulated stress. It can be offered as a quarterly, bi-annual or annual event, customized to the needs of the client.</p>
	                    		</div>
	                    		<div class="offering-tab-right col-md-6">
	                    			<div class="img-of-box">
	                    				<img src="{{ asset('images/offering/individual.jpg') }}">
	                    			</div>
	                    		</div>
	                    	</div>
	                    </div>
	                </div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- <section class="section section_offering_v3">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="second_head">Individuals</h2>
			</div>
		</div>
		<div class="row offering_v3-row">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Integral Wellbeing Coaching </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Career Transitions Coaching </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Envisioning Desired Future </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Rhythms for Wellbeing and Self-care </h4>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="section section_offering_v3">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="second_head">Group Dynamic Facilitation </h2>
			</div>
		</div>
		<div class="row offering_v3-row offering_v3-col-5">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Collaboration and Team Visioning </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Dancing Rhythms of togetherness</h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Career Lab </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Women’s Self Leadership </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Customized Wellbeing Interventions  </h4>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="section section_offering_v3 pb-100">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="second_head">Community Wellbeing initiatives </h2>
			</div>
		</div>
		<div class="row offering_v3-row">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Poorna Wellbeing circle </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Poorna wellbeing Utsava</h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Poorna wellbeing Samvaada </h4>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="#.">
					<div class="offering_v3-box">
						<div class="circle_offv3"><img src="images/properties.png"></div>
						<h4>Poorna Wellbeing Utsava for Teams </h4>
					</div>
				</a>
			</div>
		</div>
	</div>
</section> -->
<!-- <section class="section section_offering_v2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="fr_circle-box fr_circle-box1">
					<div class="fr_circle-box-inner fr_circle-box-inner1">
						<div class="fr-square"></div>
					</div>
					<div class="fr-square_text fr-square_text1"><p>Integral Wellbeing Coaching </p></div>
					<div class="fr-square_text fr-square_text2">Envisioning Desired Future </div>
					<div class="fr-square_text fr-square_text3">Rhythms for Wellbeing and Self-care </div>
					<div class="fr-square_text fr-square_text4">Career Transitions Coaching </div>
				</div>
				<div class="fr_circle-box fr_circle-box2">
					<div class="fr_circle-box-inner fr_circle-box-inner2">
						<div class="intergal-img"><img src="images/logo_main.png"></div>
					</div>
				</div>
				<div class="fr_circle-box fr_circle-box3">
					<div class="fr_circle-box-inner fr_circle-box-inner3">
						<div class="fr-square"></div>
					</div>
					<div class="fr-square_text fr-square_text1">Poorna Wellbeing circle</div>
					<div class="fr-square_text fr-square_text2">Poorna wellbeing Utsava</div>
					<div class="fr-square_text fr-square_text3">Poorna Wellbeing Utsava for Teams</div>
					<div class="fr-square_text fr-square_text4">Poorna wellbeing Samvaada </div>
				</div>
				<div class="fr_circle-box fr_circle-box4">
					<div class="fr_circle-box-inner fr_circle-box-inner4">
						<div class="star-five" id="star-five"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section section-intergal intergal-bg section_arrow">
	<div class="container">
		<div class="flex-row-center">
			<div class="col-sm-12 col-md-8 text-center">
				<div class="intergal-box">
					<h2 class="text-white wow slideInDown" data-wow-duration="0.5s" data-wow-delay="0.3s">Individuals</h2>
					<p class="wow slideInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">The four premium offerings for individuals are the USP of Poorna Wellbeing. They focus on self-awareness, sustained practice and transformation. </p>
				</div>
			</div>
			<div class="col-sm-12 col-md-4 text-center">
				<div class="square-box"><div class="intergal-img"><img src="images/logo_main.png"></div></div>
			</div>
		</div>
		
	</div>
</section>
<section class="section brown-bg section-text-content">
	<div class="container">
		<div class="row ">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="intergal-text-box">
					<h2>1. Integral Wellbeing Coaching</h2>
					<h3>Embark on a Self Mastery journey </h3>
					<p><strong><i>Needs of the client </i></strong></p>
					<ul>
						<li>Seeking to live a life of balance and personal growth</li> 
						<li>Been overwhelmed in life circumstances and lost their center and seeking their way back</li> 
						<li>Seeking to life a life of balance and personal growth </li>
						<li>Have hit a roadblock in life – at work, in relationships, in career, in personal growth and seek help to progress further </li>
					</ul>
					<p><strong><i>The coaching process</i></strong></p>
					<ul>
						<li>Help client view their life as a whole – the present, the past and future by integrating various cognitive and experiential methodologies like instruments, expressive arts – movement, theatre, visual arts, to name a few</li> 
						<li>Help client see themselves as a whole human being, focusing on the physical body, the emotional body, the intellectual body and the spiritual essence </li>
						<li>Bring holistic practices and psycho education in the session and encourage their practice in daily life to enhance self-awareness and personal mastery</li> 
					</ul>
					<p><strong><i>The results or benefits </i></strong></p>
					<ul>
						<li>Clarity on their personal vision, values and strengths</li> 
						<li>Develop dynamic self-awareness as a method to connect to their body, emotions and relationships</li>  
						<li>Experience their inner radiance and power and make it visible in the world </li>
					</ul>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-6">
				<h3 class="right_heading">Available in face to face and online mediums</h3>
				<iframe width="100%" height="330" src="https://www.youtube.com/embed/Y7fvRMsrTVA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
				<div class="owl-carousel owl-theme owl-carousel-offering">
		            <div class="item"><div class="img-box"><img src="images/offering/banner_bg1_1.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg2.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg3.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg4.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg5.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg6.jpg"></div></div>
		        </div>
			</div>
		</div>
	</div>
</section>
<section class="section intergal-bg section-text-content">
	<div class="container">
		<div class="row ">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="intergal-text-box">
					<h2>2. Career Transitions Coaching</h2>
					<h3>Embark on a transformative Career clarification journey</h3>
					<p><strong><i>Needs of the client </i></strong></p>
					<ul>
						<li>Seeking a career shift or role shift and wants to reinforce one’s natural career inclinations and talents</li> 
						<li>Experiencing lack of joy at work and seeks to find the source of dissonance and path forward </li>
						<li>Experiencing a crisis at work and wants a pause before making career choices</li> 
					</ul>
					<p><strong><i>The coaching process</i></strong></p>
					<ul>
						<li>Enable the client reflect on one’s Career Journey and discover ones Career values using the Career Anchor inventory and interview process  </li>
						<li>Identity one’s Personal Values using expressive arts methodologies </li>
					</ul>
					<p><strong><i>The results or benefits</i></strong></p>
					<ul>
						<li>Clarity and conviction on the way forward in one’s career journey</li>
					</ul>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-6">
				<h3 class="right_heading">Available in face to face and online mediums</h3>
				<iframe width="100%" height="350" src="https://www.youtube.com/embed/Y7fvRMsrTVA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
</section>
<section class="section brown-bg section-text-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="intergal-text-box">
					<h2>3.	Envisioning Desired Future </h2>
					<h3>Embark on a Future Visioning journey </h3>
					<p><strong><i>Needs of the client</i></strong></p>
					<ul>
						<li>At different life situations, client seek clarity on their life to enable them make clear and powerful life choices  </li>
						<li>Life situations could be </li>
					</ul>
					<ol>
						<li>Entered into new relationship – newly wed / becoming a parent / life after a separation. Want to create a desirable image of the future </li>
						<li>Entered a new year and want to create a powerful desired future image to take charge of life </li>
						<li>Entered into a new job </li>
						<li>High school and college Students who want to live their school and college life with awareness and focus</li> 
					</ol>
					<p><strong><i>The coaching process</i></strong></p>
					<ul>
						<li>Enable the client review and clearly see the past </li>
						<li>Journey the client to dream their desired future through non cognitive methodologies like visualization and imagination, expressive arts – visual art, movement and theatre</li>
						<li>Prototype the envisioned image through expressive arts methodologies </li>
					</ul>
					<p><strong><i>The results or benefits</i></strong></p>
					<ul>
						<li>Clear and compelling image of the desired future </li>
						<li>Clear first steps and the next few steps articulated by the client </li>
					</ul>

				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-6">
				<h3 class="right_heading">Available in face to face and online mediums</h3>
				<iframe width="100%" height="250" src="https://www.youtube.com/embed/Y7fvRMsrTVA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
				<div class="owl-carousel owl-theme owl-carousel-offering">
		            <div class="item"><div class="img-box"><img src="images/offering/banner_bg1_1.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg2.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg3.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg4.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg5.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg6.jpg"></div></div>
		        </div>
			</div>
		</div>
	</div>
</section>
<section class="section intergal-bg section-text-content">
	<div class="container">
		<div class="row ">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="intergal-text-box">
					<h2>4.	Rhythms for Wellbeing and Self-care </h2>
					<h3>Develop Resilience and Presence through Holistic Wellbeing Rhythms</h3>
					<p><strong><i>Needs of the client </i></strong></p>
					<ul>
						<li>Begin the process of Wellbeing and Self Care but needs help and support to make that beginning</li>
						<li>To emerge out of a major life crisis through sustained Wellbeing practices To emerge out of a major life crisis through sustained Wellbeing practices </li>
						<li>To bring and adhere to a Wellbeing discipline now and forever in their life</li>
					</ul>
					<p><strong><i>The rhythmic process</i></strong></p>
					<ul>
						<li>Conversation with the client to understand the current state and desired state and contract</li>
						<li>Co-create with the client commit to the weekly rhythms. At a time only one week’s rhythms are decided upon</li> 
						<li>Communication with client every evening through whatsapp message and at the end of every week on a video call to review the impact and progress on the client</li>
						<li>Conclusion of the program (4 weeks ) and way forward </li>
					</ul>
					<p><strong><i>The results or benefits </i></strong></p>
					<ul>
						<li>In charge of ones physical, emotional and spiritual self </li>
						<li>Joy of practicing wellbeing consciousness and self care</li>
						<li>Heightened self-awareness and resilience </li>
					</ul>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-6">
				<h3 class="right_heading">Available in face to face and online mediums</h3>
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/Y7fvRMsrTVA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
				<div class="owl-carousel owl-theme owl-carousel-offering">
		            <div class="item"><div class="img-box"><img src="images/offering/banner_bg1_1.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg2.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg3.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg4.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg5.jpg"></div></div>
	            	<div class="item"><div class="img-box"><img src="images/offering/banner_bg6.jpg"></div></div>
		        </div>
			</div>
		</div>
	</div>
</section> -->
<footer class="footer section footer-bg">
	<div class="footer-top section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<ul class="footer-social-link">
						<li><a href="#."><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<!-- <li><a href="#."><i class="fa fa-twitter" aria-hidden="true"></i></a></li> -->
						<li><a href="#."><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="#."><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						<li><a href="#."><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<!-- <div class="col-xs-12 col-sm-6 col-md-6">
					<div class="subscribe-newslatter">
						<form>
							<div class="form-group">
							    <input type="text" class="form-control" id="name" placeholder="Subscribe to our newsletter">
							    <button type="submit" name="submit" class="btn btn-newslatter">Subscribe</button>
						  	</div>
						</form>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<div class="section footer-logo">
		<img src="images/logo_main.png">
	</div>
	<div class="footer-bottom section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-left">
					<!-- <h3>ABOUT FOUNDER – DEEPA MAHESH</h3>
					<div class="footer-text">
						<p>Deepa Mahesh is the founder of Poorna Wellbeing, founded to spread the importance of Integral Wellbeing, balance and personal growth enabling humanity to live fulfilling and impactful lives. Deepa practices as an Integral Wellbeing Coach, where she integrates expressive arts, psychology, human biography work, movement & dance, gestalt, Bach flower remedy.... <a href="#.">Read More</a></p>
					</div> -->
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-right">
					<!-- <h3>Contact With Poorna Wellbeing</h3>
					<div class="footer-contact">
						<address>Room no. 14, Montfort Spirituality Centre, <br>
						184, Old Madras Road, Indira Nagar, <br>
						Near Vivekananda Metro Station, 
						Bengaluru - 560038</address>
						<ul class="contact-items">
							<li><a href="emailto:deepa@domain.com"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>deepa@domain.com</span></a></li>
							<li><a href="emailto:deepa@domain.com"><i class="fa fa-phone" aria-hidden="true"></i> <span>+91 96860 51005</span></a></li>
						</ul>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- <div class="facade">
	<img src="https://atmatva.com/src/img/lotus1.png" class="lotus">
</div> -->
<!-- <div class="facade facade-right">
	<img src="https://atmatva.com/src/img/lotus1.png" class="lotus">
	<img src="images/pagebg1.jpg" class="lotus">
</div> -->

<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.lettering.js"></script>
<script src="js/circletype.min.js"></script>


<script type="text/javascript">
	num = $('.header_bottom').offset().top + 80;
	$(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
	    wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
	      wow.init();
	});
</script>
<!-- scroll dwon -->
<script type="text/javascript">
	function headerHeightCalc(){
	  var getNiknkHeader = jQuery('#header_bottom').outerHeight();
	  var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
	  var calcHeaderHei = getNiknkHeader + getwpAdminbar;
	  return calcHeaderHei;
	}
	$('.pw-scrollDown button').click(function(){
	  var sectionOffset = headerHeightCalc();
	  var getHrefVal = jQuery(this).attr('scroll-data-id');
	  if(getHrefVal == '#header_bottom'){
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 0},
	        'slow');
	  }else{
	  	$('html, body').animate({
	        scrollTop: jQuery(getHrefVal).offset().top - 78},
	        'slow');
	  }
	  
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
      $('.owl-carousel-offering').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          1000: {
            items: 1,
            nav: true,
          }
        }
      })
    })
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.offering-tab-wrap ul.nav>li').each(function(){
			var tavA = $(this).find('>a');
			var tavThis = $(this);
			$(tavA).on('click', function(){
				$('.pb-nav-dropdown>li').removeClass('active');
				$('.offering-tab-wrap ul.nav>li a i').removeClass('fa-minus');
				$('.offering-tab-wrap ul.nav>li a i').addClass('fa-plus');
				$(tavA).find('i').addClass('fa-minus');
				$(tavThis).find('.pb-nav-dropdown>li:first-child a').trigger('click');
				$('.pb-nav-dropdown').slideUp();
				$(this).parent().find('.pb-nav-dropdown').slideDown();
			});
		});
		$('.pb-nav-dropdown>li').each(function(){
			var tavA = $(this).find('a');
			$(tavA).on('click', function(){
				$('.pb-nav-dropdown>li').removeClass('active');
				$('.offering-tab-wrap .tab-content .tab-pane').removeClass('active');
				$('.offering-tab-wrap .tab-content .tab-pane').removeClass('active_is');
				$('.offering-tab-wrap .tab-content .tab-pane').slideUp();
				//$('.offering-tab-wrap .tab-content .tab-pane').slideDown('active');
				
				var activeId = $(this).attr('data-href');
				$(activeId).slideDown();
				//$(this).addClass('active');
				$(activeId).addClass('active_is');
				
				//alert(activeId);
			});
		});
	});
</script>
<style type="text/css">
	.pb-header-main .navbar-nav>li>a{color: #fff;}
	
</style>
</body>
</html>

