<!DOCTYPE html>
<html lang="en">
<head>
  <title>Poorna Wellbeing</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style type="text/css">
      section.poorna-login-page .container{
        padding-top: 70px;
        padding-bottom: 70px;
      }
      .login--tab .nav-tabs{
        border:none;
      }
      .login--tab .nav-tabs > li.active a, .login--tab .nav-tabs > li.active a:hover, .nav-tabs > li.active a:focus {
        color: #111111;
        background-color: #f8f8f8;
      }
      .login--tab .nav-tabs > li {
        text-align: center;
      }
      .login--tab .nav-tabs > li {
        float: left;
        margin-bottom: -1px;
      }
      .tab-content {
        padding: 15px;
        border: 1px solid #eeeeee;
      }
      .pt-5 {
       padding-top: 5px !important;
      }
      form label {
        font-weight: bold;
        line-height: 1.7;
        color: #777777;
      }
      .form-control {
        border-radius: 0;
        box-shadow: none;
        height: 45px;
      }
      .checkbox label, .radio label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 0;
        font-weight: 400;
        cursor: pointer;
      }
      .text-theme-colored {
        color: #00a3c8 !important;
      }
      .btn-dark {
        color: #fff;
      background-color: #222222;
      border-color: #222222;
    }
    .btn-dark:hover {
    color: #fff;
    background-color: #090909;
    border-color: #040404;
}
    </style>
</head>
<body>
<!-- bottom navigation -->
<div class="header_bottom section_arrow trans_header" id="header_bottom">
    @include('nav.menu')
</div>
<section class="section-event_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="event-banner_head">
          <p>Poorna Wellbeing</p>
          <h1>Login</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="poorna-login-page">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 login--tab">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#login-tab" data-toggle="tab" aria-expanded="true">Login</a></li>
              <li class=""><a href="#register-tab" data-toggle="tab" aria-expanded="false">Register</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade p-15 active in" id="login-tab">
                <h4 class="text-gray mt-0 pt-5"> Login</h4>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur elit.</p>
                <form name="login-form" class="clearfix" method="POST" action="{{ route('login') }}">
                   @csrf
                  <div class="row">
                    <div class="form-group col-md-12">
                      <label for="email">{{ __('Username/E-Mail Address') }}</label>
                      <input id="form_username_email" name="email" class="form-control" type="text" 
                      @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                      @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-12">
                      <label for="form_password">{{ __('Password') }}</label>
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                  </div>
                  <div class="checkbox pull-left mt-15">
                    <label for="form_checkbox">
                      <input id="form_checkbox" name="form_checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                      {{ __('Remember Me') }} </label>
                  </div>
                  <div class="form-group pull-right mt-10">
                    <button type="submit" class="btn btn-dark btn-sm">{{ __('Login') }}</button>
                  </div>
                  <div class="clear text-center pt-10">
                    @if (Route::has('password.request'))
                    <a class="text-theme-colored font-weight-600 font-12" href="#">{{ __('Forgot Your Password?') }}</a>
                    @endif
                  </div>
                </form>
              </div>
              <div class="tab-pane fade p-15" id="register-tab">
                <form name="reg-form" class="register-form" method="post" action="{{ route('register') }}">
                  @csrf
                  <div class="icon-box mb-0 p-0">
                    <a href="#" class="icon icon-bordered icon-rounded icon-sm pull-left mb-0 mr-10">
                      <i class="pe-7s-users"></i>
                    </a>
                    <h4 class="text-gray pt-10 mt-0 mb-30">Don't have an Account? Register Now.</h4>
                  </div>
                  <hr>
                  <p class="text-gray"></p>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="form_name">{{ __('Name') }}</label>
                      <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                      @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                    <div class="form-group col-md-6">
                      <label>Email Address</label>
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                      @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label for="password">{{ __('Password') }}</label>
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                      @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                    <div class="form-group col-md-6">
                      <label>{{ __('Confirm Password') }}</label>
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                  </div>
                  <div class="form-group">
                    <button class="btn btn-dark btn-lg btn-block mt-15" type="submit">{{ __('Register') }}</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<!-- Footer -->
@include('layouts.footer')
<!-- END -->


<!-- script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>


<script type="text/javascript">
  num = $('.header_bottom').offset().top + 80;
  $(window).bind('scroll', function() {
         if ($(window).scrollTop() > num) {
             $('.header_bottom').addClass('fixed');
             $('.section-intergal').addClass('intergal-padding');
         }
         else {
             num = $('.header_bottom').offset().top;
             $('.header_bottom').removeClass('fixed');
             $('.section-intergal').removeClass('intergal-padding');
         }
    });
    $(document).ready(function(){
      wow = new WOW({boxClass: 'wow', animateClass: 'animated',offset: 0,mobile: true,live: true})
        wow.init();
  });
</script>
<!-- scroll dwon -->
<script type="text/javascript">
  function headerHeightCalc(){
    var getNiknkHeader = jQuery('#header_bottom').outerHeight();
    var getwpAdminbar = jQuery('#wpadminbar').outerHeight();
    var calcHeaderHei = getNiknkHeader + getwpAdminbar;
    return calcHeaderHei;
  }
  $('.pw-scrollDown button').click(function(){
    var sectionOffset = headerHeightCalc();
    var getHrefVal = jQuery(this).attr('scroll-data-id');
    if(getHrefVal == '#header_bottom'){
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 0},
          'slow');
    }else{
      $('html, body').animate({
          scrollTop: jQuery(getHrefVal).offset().top - 78},
          'slow');
    }
    
  });
</script>

<script type="text/javascript">
  
  $('.counter').counterUp({
    delay: 10,
    time: 2000
  });
  $('.counter').addClass('animated fadeInDownBig');
  $('h3').addClass('animated fadeIn');
</script>
<style type="text/css">
  .pb-header-main .navbar-nav>li>a{color: #fff;}
  /*canvas#bg {
      background: linear-gradient(90deg, rgba(224,19,19,1) 0%, rgba(6,84,171,1) 22%, rgba(255,187,13,1) 57%, rgba(0,212,255,1) 82%, rgba(48,244,13,1) 100%);
  }*/
</style>
</body>
</html>