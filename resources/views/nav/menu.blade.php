<style type="text/css">
  /* ========================================================================
 * Create reza ardi
 * Copyright 2017
 * ======================================================================== */
@import url(https://fonts.googleapis.com/css?family=Saira+Semi+Condensed:100,400,700);
/*body {
  background-color: #fff;
  text-align: center;
  font-family: 'Saira Semi Condensed', sans-serif;
}

.nav-inline {
  padding: 0;
}
.nav-inline .navbar-nav .nav-item {
  position: static;
}
.nav-inline .navbar-nav .nav-item .nav-link {
  padding: 15px;
}
.nav-inline .navbar-nav .nav-item .nav-link[aria-expanded="true"] {
  background-color: #f2f2f2;
}
.nav-inline .navbar-nav .nav-item.show:before {
  content: " ";
  display: block;
  background: #f2f2f2;
  position: absolute;
  width: 100%;
  height: 50px;
  top: 100%;
  right: 0;
  left: 0;
}
@media (max-width: 767px) {
  .nav-inline .navbar-nav .nav-item.show:before {
    display: none;
  }
}
.nav-inline .navbar-nav .dropdown-menu {
  left: 0 !important;
  right: 0 !important;
  box-shadow: none;
  border: none;
  margin: 0 auto;
  max-width: 1140px;
  background: transparent;
  padding: 0;
}
.nav-inline .navbar-nav .dropdown-menu.show {
  display: flex;
}
.nav-inline .navbar-nav .dropdown-menu .dropdown-item {
  width: auto;
  padding: 13px 15px;
}*/

/*.title {
  font-weight: bold;
  line-height: 1;
  margin-top: 40px;
}
.title span {
  font-weight: 100;
}*/


</style>
<nav class="navbar">
	 <ul class="nav navbar-nav navbar-right">
    <!-- <li class="navbar-brand logo"><a class="navbar-brand" href="#">
    	<img src="{{ asset('images/logo_main.png') }}"></a>
    </li> -->
    <li class="{{ (\Request::route()->getName() == '') ? 'active' : '' }}">
    	<a href="/">Home</a></li>
    <!-- <li><a href="#">About us</a></li> -->
    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" id="navbarDropdown" href="{{route('about')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About Us <i class="fa fa-caret-down"></i></a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{route('about')}}#section-our-story">Our Story</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route('about')}}#section-our-logo">Our logo and values</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route('about')}}#section-our-aspiration">Our aspiration</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route('about')}}#section-our-team">Meet our team</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route('about')}}#section-clientele">Clientele</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route('about')}}#section-engage">Engage with us</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{route('about')}}#section-collaborations">Collaborations</a>
      </div>
    </li>
    <li><a href="{{route('poornawellbeing-model')}}">Poorna Wellbeing Model</a></li>
    <li class="{{ (\Request::route()->getName() == 'offerings') ? 'active' : '' }}">
        <a href="{{route('offerings')}}">Offerings</a></li>
    <li class="{{ (\Request::route()->getName() == 'events') ? 'active' : '' }} nav-item dropdown"><a href="{{route('events')}}" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Events <i class="fa fa-caret-down"></i></a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#">Gallery</a>
      </div>
    </li>
    <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Resources <i class="fa fa-caret-down"></i></a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#">Blogs</a>
      </div>
    </li>
    <li class="{{ (\Request::route()->getName() == 'contact') ? 'active' : '' }}">
        <a href="{{route('contact')}}">Contact</a></li>
  <!--  <li>
        @guest
        <a href="javascript:void(0);" data-toggle="modal" data-target="#LoginModal">Login</a>
        @else
        <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
        </form>
        @endguest
    </li> -->
    <!-- <li class="assess-li">
    	<a href="{{route('login')}}" class="btn btn-assess">ASSESS YOUR POORNA(HOLISTIC) WELLBEING </a>
    </li> -->
</ul>
