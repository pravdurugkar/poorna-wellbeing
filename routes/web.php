<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

/**
 * Main Pages 
 * 
 * Static Pages
 */
Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');

Route::get('/poornawellbeing-model', function () {
    return view('pages.poornawellbeing-model');
})->name('poornawellbeing-model');

Route::get('/terms', function () {
    return view('pages.terms');
});

Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/privacy-policy', function () {
    return view('pages.privacy-policy');
});

Route::get('/return-refund-policy', function () {
    return view('pages.return-refund-policy');
});

Route::get('/offerings', 'ViewController@page')->name('offerings');

/**
 * Dynamic Pages
 */
Route::get('/events', 'EventsController@page')->name('events');
Route::get('/event/{parmalink}', 'EventsController@singleEvent')->name('event');
Route::get('/event/{parmalink}/booking', 'EventsController@bookingForm')->name('eventBooking');
Route::post('/event/{parmalink}/booking', 'EventsController@bookEvent')->name('eventBooking');
Route::group(['as' => 'products.', 'prefix' => 'products'], function () {
	Route::get('/{product}', 'ProductsController@single')->name('single');
    Route::post('/addToCart/{product}','ProductsController@addToCart')->name('addToCart');
});
Route::get('/orders','OrderController@index')->name('orders');
Route::post('shipping','OrderController@store')->name('orders.shipping');


